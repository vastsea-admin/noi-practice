#include<iostream>
#include<cmath>
using namespace std;
void exchange(int *x,int *y);
int main() {
	int a,b;
	cout<<"a=";
	cin>>a;
	cout<<endl;
	cout<<"b=";
	cin>>b;
	cout<<endl;
	exchange(&a,&b);
	cout<<"a="<<a<<endl<<"b="<<b<<endl;
	return 0;
}
void exchange(int *x,int *y) {
	int tmp=0;
	tmp=*y;
	*y=*x;
	*x=tmp;
}

