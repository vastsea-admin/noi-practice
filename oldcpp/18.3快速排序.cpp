#include<iostream>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<cstdio>
#include<math.h>
using namespace std;

void QuickSort( int *a,int left, int right) {
	int i,j,middle,temp;
	i = left;
	j = right;
	middle = a[ (left+right)/2 ];
	do {
		while( a[i]<middle && i<right ) //从左扫描大于中值的数
			i++;
		while( a[j]>middle && j>left ) //从右扫描小于中值的数
			j--;
		if( i<=j ) { //找到了一对值
			temp = a[i];
			a[i] = a[j];
			a[j] = temp;
			i++;
			j--;
		}
	} while ( i<j ); //如果两边的下标交错，就停止(完成一次)
//当左半边有值（left<j）,递归左半边
	if( left < j )
		QuickSort( a, left, j);
//当右半边有值（right>i）,递归右半边
	if( i < right )
		QuickSort( a, i, right);
}
const int num=99999;
int main()
{
	srand(time(0)); 
	int in[num];
	int jishu1=0,jishu2=0;
	for(int i=0; i<num; i++) { //随机数赋值in
		in[i]=10+rand()%500000;
	}
	QuickSort(in,0,99998);
		for(long int i=0; i<num; i++)	{ //值输出
		cout<<"第"<<i+1<<"个数"<<endl;
		cout<<in[i]<<endl;
	}
	return 0;
}

