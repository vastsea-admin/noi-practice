# Microsoft Developer Studio Generated NMAKE File, Based on 斗地主.dsp
!IF "$(CFG)" == ""
CFG=斗地主 - Win32 Debug
!MESSAGE No configuration specified. Defaulting to 斗地主 - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "斗地主 - Win32 Release" && "$(CFG)" != "斗地主 - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "斗地主.mak" CFG="斗地主 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "斗地主 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "斗地主 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "斗地主 - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\斗地主.exe"


CLEAN :
	-@erase "$(INTDIR)\Client_Game.obj"
	-@erase "$(INTDIR)\Dlg_Start.obj"
	-@erase "$(INTDIR)\Draw_Cards_Engine.obj"
	-@erase "$(INTDIR)\Draw_Item_Engine.obj"
	-@erase "$(INTDIR)\Game.obj"
	-@erase "$(INTDIR)\Link.obj"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\My_DirectDraw.obj"
	-@erase "$(INTDIR)\My_DirectInput.obj"
	-@erase "$(INTDIR)\Playing_Cards.obj"
	-@erase "$(INTDIR)\Server_Game.obj"
	-@erase "$(INTDIR)\Single_Game.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\斗地主.obj"
	-@erase "$(INTDIR)\斗地主.pch"
	-@erase "$(INTDIR)\斗地主.res"
	-@erase "$(OUTDIR)\斗地主.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)\斗地主.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\斗地主.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\斗地主.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\斗地主.pdb" /machine:I386 /out:"$(OUTDIR)\斗地主.exe" 
LINK32_OBJS= \
	"$(INTDIR)\Client_Game.obj" \
	"$(INTDIR)\Dlg_Start.obj" \
	"$(INTDIR)\Draw_Cards_Engine.obj" \
	"$(INTDIR)\Draw_Item_Engine.obj" \
	"$(INTDIR)\Game.obj" \
	"$(INTDIR)\Link.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\My_DirectDraw.obj" \
	"$(INTDIR)\My_DirectInput.obj" \
	"$(INTDIR)\Playing_Cards.obj" \
	"$(INTDIR)\Server_Game.obj" \
	"$(INTDIR)\Single_Game.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\斗地主.obj" \
	"$(INTDIR)\斗地主.res"

"$(OUTDIR)\斗地主.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\斗地主.exe" "$(OUTDIR)\斗地主.bsc"


CLEAN :
	-@erase "$(INTDIR)\Client_Game.obj"
	-@erase "$(INTDIR)\Client_Game.sbr"
	-@erase "$(INTDIR)\Dlg_Start.obj"
	-@erase "$(INTDIR)\Dlg_Start.sbr"
	-@erase "$(INTDIR)\Draw_Cards_Engine.obj"
	-@erase "$(INTDIR)\Draw_Cards_Engine.sbr"
	-@erase "$(INTDIR)\Draw_Item_Engine.obj"
	-@erase "$(INTDIR)\Draw_Item_Engine.sbr"
	-@erase "$(INTDIR)\Game.obj"
	-@erase "$(INTDIR)\Game.sbr"
	-@erase "$(INTDIR)\Link.obj"
	-@erase "$(INTDIR)\Link.sbr"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MainFrm.sbr"
	-@erase "$(INTDIR)\My_DirectDraw.obj"
	-@erase "$(INTDIR)\My_DirectDraw.sbr"
	-@erase "$(INTDIR)\My_DirectInput.obj"
	-@erase "$(INTDIR)\My_DirectInput.sbr"
	-@erase "$(INTDIR)\Playing_Cards.obj"
	-@erase "$(INTDIR)\Playing_Cards.sbr"
	-@erase "$(INTDIR)\Server_Game.obj"
	-@erase "$(INTDIR)\Server_Game.sbr"
	-@erase "$(INTDIR)\Single_Game.obj"
	-@erase "$(INTDIR)\Single_Game.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\斗地主.obj"
	-@erase "$(INTDIR)\斗地主.pch"
	-@erase "$(INTDIR)\斗地主.res"
	-@erase "$(INTDIR)\斗地主.sbr"
	-@erase "$(OUTDIR)\斗地主.bsc"
	-@erase "$(OUTDIR)\斗地主.exe"
	-@erase "$(OUTDIR)\斗地主.ilk"
	-@erase "$(OUTDIR)\斗地主.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\斗地主.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\斗地主.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\斗地主.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Client_Game.sbr" \
	"$(INTDIR)\Dlg_Start.sbr" \
	"$(INTDIR)\Draw_Cards_Engine.sbr" \
	"$(INTDIR)\Draw_Item_Engine.sbr" \
	"$(INTDIR)\Game.sbr" \
	"$(INTDIR)\Link.sbr" \
	"$(INTDIR)\MainFrm.sbr" \
	"$(INTDIR)\My_DirectDraw.sbr" \
	"$(INTDIR)\My_DirectInput.sbr" \
	"$(INTDIR)\Playing_Cards.sbr" \
	"$(INTDIR)\Server_Game.sbr" \
	"$(INTDIR)\Single_Game.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\斗地主.sbr"

"$(OUTDIR)\斗地主.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=Dxguid.lib DDraw.lib Dinput.lib Dsound.lib Winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\斗地主.pdb" /debug /machine:I386 /out:"$(OUTDIR)\斗地主.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\Client_Game.obj" \
	"$(INTDIR)\Dlg_Start.obj" \
	"$(INTDIR)\Draw_Cards_Engine.obj" \
	"$(INTDIR)\Draw_Item_Engine.obj" \
	"$(INTDIR)\Game.obj" \
	"$(INTDIR)\Link.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\My_DirectDraw.obj" \
	"$(INTDIR)\My_DirectInput.obj" \
	"$(INTDIR)\Playing_Cards.obj" \
	"$(INTDIR)\Server_Game.obj" \
	"$(INTDIR)\Single_Game.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\斗地主.obj" \
	"$(INTDIR)\斗地主.res"

"$(OUTDIR)\斗地主.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("斗地主.dep")
!INCLUDE "斗地主.dep"
!ELSE 
!MESSAGE Warning: cannot find "斗地主.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "斗地主 - Win32 Release" || "$(CFG)" == "斗地主 - Win32 Debug"
SOURCE=.\Client_Game.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Client_Game.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Client_Game.obj"	"$(INTDIR)\Client_Game.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Dlg_Start.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Dlg_Start.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Dlg_Start.obj"	"$(INTDIR)\Dlg_Start.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Draw_Cards_Engine.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Draw_Cards_Engine.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Draw_Cards_Engine.obj"	"$(INTDIR)\Draw_Cards_Engine.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Draw_Item_Engine.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Draw_Item_Engine.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Draw_Item_Engine.obj"	"$(INTDIR)\Draw_Item_Engine.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Game.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Game.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Game.obj"	"$(INTDIR)\Game.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Link.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Link.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Link.obj"	"$(INTDIR)\Link.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\MainFrm.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\MainFrm.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\MainFrm.obj"	"$(INTDIR)\MainFrm.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\My_DirectDraw.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\My_DirectDraw.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\My_DirectDraw.obj"	"$(INTDIR)\My_DirectDraw.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\My_DirectInput.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\My_DirectInput.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\My_DirectInput.obj"	"$(INTDIR)\My_DirectInput.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Playing_Cards.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Playing_Cards.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Playing_Cards.obj"	"$(INTDIR)\Playing_Cards.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Server_Game.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Server_Game.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Server_Game.obj"	"$(INTDIR)\Server_Game.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\Single_Game.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\Single_Game.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\Single_Game.obj"	"$(INTDIR)\Single_Game.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "斗地主 - Win32 Release"

CPP_SWITCHES=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)\斗地主.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\斗地主.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"

CPP_SWITCHES=/nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\斗地主.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\斗地主.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=".\斗地主.cpp"

!IF  "$(CFG)" == "斗地主 - Win32 Release"


"$(INTDIR)\斗地主.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ELSEIF  "$(CFG)" == "斗地主 - Win32 Debug"


"$(INTDIR)\斗地主.obj"	"$(INTDIR)\斗地主.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\斗地主.pch"


!ENDIF 

SOURCE=".\斗地主.rc"

"$(INTDIR)\斗地主.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

