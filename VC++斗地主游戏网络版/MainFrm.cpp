//Download by http://www.NewXing.com
// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "斗地主.h"

#include "Game.h"
#include "Single_Game.h"
#include "Client_Game.h"
#include "Server_Game.h"

//#include "My_DirectInput.h"
//#include "my_directdraw.h"

#include "Draw_Cards_Engine.h"


#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	Create(NULL,"斗地主",WS_POPUP);  //终于知道了!在OnCreate()就会用到pCMainFrame!!!!!!!!1
//	pCMainFrame = (CMainFrame*)this;
}

CMainFrame::~CMainFrame()
{
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CWindowDC	dc(this);

	int x = dc.GetDeviceCaps(HORZRES);
	int y = dc.GetDeviceCaps(VERTRES);

	if(x == 800 && y == 600)
	{
		//改变所有的全局变量
		AfxMessageBox("对不起!本游戏默认分辨率为1024*768,如果在800*600\
						分辨率下可能会显示不正常!我将尽快解决此问题!");
	}
	else if(x != 1024 || y != 768)
	{
		AfxMessageBox("对不起!本游戏只支持800*600和1024*768两种分辨率!");
		PostMessage(WM_CLOSE );
		return 0;
	}
//取得游戏主窗口类指针;
	pCMainFrame = (CMainFrame*)this;
	::hWnd = this->m_hWnd;

//产生游戏主体框架类对象;
	if( pCenterPlayer->m_nPlayerType == 0)
	{
		pGame = new CSingleGame;
	}
	else if( pCenterPlayer->m_nPlayerType == 1)
	{
		pGame = new CServerGame;
	}
	else if( pCenterPlayer->m_nPlayerType == 2 )
	{
		pGame = new CClientGame;
	}

	//初始化游戏类,并产生其他几个类;
	pGame->Init();

	::SetTimer(hWnd,0,0,NULL);   //开始游戏;
	//用户操作界面线程;
	::CreateThread(NULL,0,pGame->RefreshMenu,0,0,&dwThreadId);
	return 0;
}
//定时处理;
void CMainFrame::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 0)  
	{
		::KillTimer(hWnd,0);
		pGame->Run();    //开始游戏;
	}
	else if(nIDEvent == 20)  //
	{
		pGame->RefreshMyTurn();  //基类 ;
	}
	CFrameWnd::OnTimer(nIDEvent);
}

void CMainFrame::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if( nChar== VK_ESCAPE )    
		PostMessage(WM_CLOSE ); 
	
	CFrameWnd::OnChar(nChar, nRepCnt, nFlags);
}
