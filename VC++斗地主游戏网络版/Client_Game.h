//Download by http://www.NewXing.com
// Client_Game.h: interface for the CClient_Game class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENT_GAME_H__DE1855D6_81E0_48D8_98DB_96C83D0C355D__INCLUDED_)
#define AFX_CLIENT_GAME_H__DE1855D6_81E0_48D8_98DB_96C83D0C355D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CClientGame : public CGame  
{
private:
	int DecideLord();			//决定地主;

	virtual int LeftDoing();		//左边玩家（电脑）思考，并决定要出的牌;
	virtual int CenterDoing();		//中间(本机)玩家出牌;
	virtual int RightDoing();		//右边玩家（电脑）思考，并决定要出的牌;
	//奇怪!居然可以将虚拟函数声明成私有?
	virtual int Run();			//整个游戏过程的中枢;
public:
	CClientGame();
	virtual ~CClientGame();
};

#endif // !defined(AFX_CLIENT_GAME_H__DE1855D6_81E0_48D8_98DB_96C83D0C355D__INCLUDED_)
