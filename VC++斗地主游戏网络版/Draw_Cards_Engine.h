//Download by http://www.NewXing.com
// Draw_Cards_Engine.h: interface for the CDraw_Cards_Engine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DRAW_CARDS_ENGINE_H__B3E7EB3D_BD8D_42F0_B397_F9CC356B4115__INCLUDED_)
#define AFX_DRAW_CARDS_ENGINE_H__B3E7EB3D_BD8D_42F0_B397_F9CC356B4115__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//扑克的位置;
class CCardPos
{
public:
	int m_nX;  //
	int m_nY;
	BOOL m_bVisual; //是否可见,1为可见,0为不可见;
public:
	CCardPos();
};
//扑克牌布局地图结构;
class CCardsMap
{
public:
	int m_nUpCounter;
	CCardPos Center0[20];  //中间未出的牌;
	CCardPos Center1[20];  //中间已经出的牌;
	CCardPos Center2[20];  //中间刚出的牌;

	CCardPos Left0[20];  //左边未出的牌;
	CCardPos Left1[20];  //左边已经出的牌;
	CCardPos Left2[20];  //左边刚出的牌;

	CCardPos Right0[20];  //右边未出的牌;
	CCardPos Right1[20];  //右边已经出的牌;
	CCardPos Right2[20];  //右边刚出的牌;

	int m_nLordCards;		//1表示显示牌背面,2表示显示牌面;
	CCardPos m_cLordCards[3];   //三张地主牌;

	CCardPos m_cDistributeCard;   //
public:
	CCardsMap();
};

class CDrawCardsEngine  
{
private:
	//存放扑克的离屏表面;
	LPDIRECTDRAWSURFACE7  lpddsbg_Cards[15];
	//精灵颜色键;
	const DWORD BLUE;
private:
	int DrawCard(int x,
					int y,
					int Color,
					int Value,
					LPDIRECTDRAWSURFACE7 lpdds_Dest = lpddsback ); 

	int MoveCard(int Dest_x,
					int Dest_y,
					int Source_x,
					int Source_y,
					int Card_Color,
					int Card_Value,
					int Speed = 140);		//移动速度,以象素为单位;
public:
	int m_bVisual;          //牌是否可见;
public:
	CDrawCardsEngine();
	virtual ~CDrawCardsEngine();

	int Init();

	void Redraw(LPDIRECTDRAWSURFACE7  pSurface = lpddsback);

	int DrawCleanUp();		//中间(本机)玩家更新牌面并依次陈列出来;	

	int Dealing();			//发牌;
	int DealingLord(int);	//发地主牌;

	int CenterDiscarding();      //本机玩家出牌;
	int CenterBacking();		 //本机玩家刚出的牌从刚出牌区移动到已经出牌区;

	int LeftDiscarding();	    //左边玩家出牌;
	int LeftBacking();			//左边玩家刚出的牌从刚出牌区移动到已经出牌区;

	int RightDiscarding();	    //右边玩家出牌;
	int RightBacking();			//右边玩家刚出的牌从刚出牌区移动到已经出牌区;
};

#endif // !defined(AFX_DRAW_CARDS_ENGINE_H__B3E7EB3D_BD8D_42F0_B397_F9CC356B4115__INCLUDED_)
