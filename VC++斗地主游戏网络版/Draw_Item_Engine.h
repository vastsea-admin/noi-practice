//Download by http://www.NewXing.com
// Draw_Item_Engine.h: interface for the CDraw_Item_Engine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DRAW_ITEM_ENGINE_H__915636FB_44F9_4464_A4F4_CFC2FC6AE70C__INCLUDED_)
#define AFX_DRAW_ITEM_ENGINE_H__915636FB_44F9_4464_A4F4_CFC2FC6AE70C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDrawItemEngine  
{
private:
	DWORD blue;	
	//存放桌面精灵的离屏表面;
	LPDIRECTDRAWSURFACE7  lpddsbg_Person[6]; 
	LPDIRECTDRAWSURFACE7  lpddsbg_Menu[6]; 
public:

	CDrawItemEngine();
	virtual ~CDrawItemEngine();
public:
	int Init();

 	void Redraw(LPDIRECTDRAWSURFACE7  pSurface = lpddsback);
	//产生随机地主动画的函数
	int GameCounter(int nGame_Counter);		//显示局数;
	int BringRandLord(int nDefaultLord);		//产生随机地主的动画;
	int ChooseLord();				   //选择是否当地主;
	int GameInfo(int nPlayer,int nInfoType);   
	//显示得分情况;
	int GameScore(int nCurrentLord,int nLeft,int nCenter,int nRight);
};

#endif // !defined(AFX_DRAW_ITEM_ENGINE_H__915636FB_44F9_4464_A4F4_CFC2FC6AE70C__INCLUDED_)
