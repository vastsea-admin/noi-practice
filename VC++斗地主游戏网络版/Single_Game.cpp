//Download by http://www.NewXing.com
// Single_Game.cpp: implementation of the CSingleGame class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "斗地主.h"
#include "MainFrm.h"

#include "My_DirectInput.h"
#include "my_directdraw.h"

#include "Playing_Cards.h"

#include "Game.h"

#include "Draw_Item_Engine.h"
#include "Draw_Cards_Engine.h"

#include "Single_Game.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSingleGame::CSingleGame()
{

}

CSingleGame::~CSingleGame()
{

}

//决定地主 ;
int CSingleGame::DecideLord()
{
	//产生地主;
	m_nDefaultLord =	rand()%3;
//	m_nDefaultLord = 2;
	pDrawItem->BringRandLord(m_nDefaultLord);
	m_nCurrentLord = m_nDefaultLord;

	int High = -1;

	int My_Cards_Value = pCenterCards->AccountCardsValue();
	int Left_Cards_Value = pLeftCards->AccountCardsValue();
	int Right_Cards_Value = pRightCards->AccountCardsValue();

	if( My_Cards_Value >= Right_Cards_Value )
	{
		if( My_Cards_Value >= Left_Cards_Value)
		{
			High = 1;
		}
		else
		{
			High = 0;
		}
	}
	else
	{
		if( Right_Cards_Value >= Left_Cards_Value )
		{
			High = 2;
		}
		else
		{
			High = 0;
		}
	}  //end if;

//	char info[50];
//	sprintf(info,"Left:%d Center:%d Right:%d\n",\
//				Left_Cards_Value,My_Cards_Value,Right_Cards_Value);
//	AfxMessageBox(info);
//	Sleep(10000);
//	pDraw->DebugText(info,1000);

	int	counter = 0;
while(1)
{
	if( m_nCurrentLord ==1 )
	{
		while(1)
		{
			pDrawItem->ChooseLord();
			if( pInput->IsLButtonDown(m_cYES_RECT) )  //要
			{
				//相关处理;
				PlaySound(MAKEINTRESOURCE(IDR_CHOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				pDrawItem->GameInfo(m_nCurrentLord,0);
				Sleep(500);   //产生一个延时;
				goto out;
			}
			else if( pInput->IsLButtonDown(m_cNO_RECT) )  //不要
			{
				//相关处理;
				PlaySound(MAKEINTRESOURCE(IDR_CHOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				counter++;
				if( counter == 3 )
				{
					m_nCurrentLord = m_nDefaultLord;
					pDrawItem->GameInfo(m_nDefaultLord,2);
					goto out;
				}
				pDrawItem->GameInfo(m_nCurrentLord,1);
				Sleep(500);   //产生一个延时;
				m_nCurrentLord = 2;
				break;
			}
			Sleep(50);   //产生一个延时;
		}
	}
	else if( m_nCurrentLord == 0 )
	{
		//根据当前牌决定是否当地主;
		if( High == 0 )   //当
		{
			pDrawItem->GameInfo(m_nCurrentLord,0);
			goto out;
		}
		else			 //不当;
		{
			counter++;
			if( counter == 3 )
			{
				m_nCurrentLord = m_nDefaultLord;
				pDrawItem->GameInfo(m_nDefaultLord,2);
				goto out;
			}
			pDrawItem->GameInfo(m_nCurrentLord,1);
			Sleep(500);   //产生一个延时;
			m_nCurrentLord = 1;
		}
	}
	else if( m_nCurrentLord == 2 )
	{
		//根据当前牌决定是否当地主;
		if( High == 2 )   //当
		{
			pDrawItem->GameInfo(m_nCurrentLord,0);
			goto out;
		}
		else			 //不当;
		{
			counter++;
			if( counter == 3 )
			{
				m_nCurrentLord = m_nDefaultLord;
				pDrawItem->GameInfo(m_nDefaultLord,2);
				goto out;
			}
			pDrawItem->GameInfo(m_nCurrentLord,1);
			Sleep(500);   //产生一个延时;
			m_nCurrentLord = 0;
		}
	}
}//end while;

out:
	return 1;
}

//int CSingleGame::Bring_Rand_Lord()

int CSingleGame::Run()
{
	//开始新的牌局;
	NewGame();		   
	//显示局数;
	pDrawItem->GameCounter(m_nGameCounter);  
	//洗牌;
	ShuffleCards();
	//发牌;
	pDrawCards->Dealing();   

	pLeftCards->ScanToTable();
	pRightCards->ScanToTable();

	//决定地主;
	DecideLord();	   

	pDrawCards->DealingLord(m_nCurrentLord); //发地主牌;

	m_bOperate  = TRUE;
//开始循环;
	if(m_nCurrentLord == 0)
	{
		LeftDoing();
		::SetTimer(hWnd,20,50,NULL);
	}
	else if(m_nCurrentLord == 1)
	{
		::SetTimer(hWnd,20,50,NULL);
	}
	else if(m_nCurrentLord == 2)
	{
		RightDoing();
		LeftDoing();
		::SetTimer(hWnd,20,50,NULL);
	}
	return 1;
}
//左边玩家（电脑）思考，并决定要出的牌;
int CSingleGame::LeftDoing()
{
	if( m_nRoundCounter == 3 )  //说明已经出了牌;
	{
		//把已经出的牌回退;
		if(pLeftCards->m_nDiscardingCounter != 0)
			pDrawCards->LeftBacking();

		if(pCenterCards->m_nDiscardingCounter != 0)
			pDrawCards->CenterBacking();

		if(pRightCards->m_nDiscardingCounter != 0)
			pDrawCards->RightBacking();

		m_nRoundCounter = 0;   //新的一圈;
	}

	m_nRoundCounter++;

	if( m_nLordLock == 0 )
	{
		return -1;
	}

	if(pRightCards->m_cDiscardingType.m_nTypeNum == 0 &&
		pCenterCards->m_cDiscardingType.m_nTypeNum == 0)
	{
		m_nLordLock = -1;   //解锁;
		//开始新的一回合,选择一种牌型出;
		pLeftCards->DirectChoose();

		if( pLeftCards->m_cDiscardingType.m_nTypeNum == 4 )
		{
			m_nBombCounter++;//加分;
		}

		pLeftCards->CleanUp();
		pDrawCards->LeftDiscarding();
		::RedrawGame();
		if( pLeftCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;			
			if(m_nCurrentLord == 2 )
			{
				PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
			}
			else
			{
				PlaySound(MAKEINTRESOURCE(IDR_LOSE),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
			}
			pDrawCards->m_bVisual = TRUE;
			AccountScore(0);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 0;
		}

		return 1;
	}	
	//1.两者都不能大牌,我是最大;
	//2.新的一回合开始,我先出;

	if(pRightCards->m_cDiscardingType.m_nTypeNum != 0 )
	{
		//如果同伙出了比较大的牌,则不跟;
		if( m_nCurrentLord == 1 && 
			m_nLordLock == 1  &&
			pRightCards->m_cDiscardingType.m_nTypeValue > 13 )
		{
			//不要;
			pLeftCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",250,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			//补丁:地主一旦过牌,本轮内都不能再出牌;
			Sleep(300);
			return 1;
		}

		if( pLeftCards->CompareChoose(pRightCards->m_cDiscardingType) )
		{
			if( pLeftCards->m_cDiscardingType.m_nTypeNum == 4 )
			{
				m_nBombCounter++;//加分;
			}
			pLeftCards->CleanUp();
			pDrawCards->LeftDiscarding();
			::RedrawGame();
			if( pLeftCards->m_nCardsCounter == 0 )
			{
				//打印结束信息;
				if(m_nCurrentLord == 2 )
				{
					PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				else
				{
					PlaySound(MAKEINTRESOURCE(IDR_LOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				pDrawCards->m_bVisual = TRUE;
				AccountScore(0);
				::SetTimer(hWnd,0,0,NULL);   //开始游戏;
				return 0;
			}
		}
		else
		{
			//补丁:地主一旦过牌,本轮内都不能再出牌;
			if( m_nCurrentLord == 0 )
			{
				m_nLordLock = 0;
			}
			//不要;
			pLeftCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",250,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);			
			Sleep(300);
		}
	}
	else
	{
		//如果同伙出了比较大的牌,则不跟;
		if( m_nCurrentLord == 2 && 
			m_nLordLock == 2 &&
			pCenterCards->m_cDiscardingType.m_nTypeValue > 13 )
		{
			//不要;
			pLeftCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",250,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			Sleep(300);
			return 1;
		}

		//如果上家没出牌,则根据上上家决定出牌;
		if( pLeftCards->CompareChoose(pCenterCards->m_cDiscardingType) )
		{
			if( pLeftCards->m_cDiscardingType.m_nTypeNum == 4 )
			{
				m_nBombCounter++;//加分;
			}
			pLeftCards->CleanUp();
			pDrawCards->LeftDiscarding();
			::RedrawGame();
			if( pLeftCards->m_nCardsCounter == 0 )
			{
				//打印结束信息;
				if(m_nCurrentLord == 2 )
				{
					PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				else
				{
					PlaySound(MAKEINTRESOURCE(IDR_LOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				pDrawCards->m_bVisual = TRUE;
				AccountScore(0);
				::SetTimer(hWnd,0,0,NULL);   //开始游戏;
				return 0;
			}
		}
		else
		{
			if( m_nCurrentLord == 0 )
			{
				m_nLordLock = 0;
			}
			//不要;
			pLeftCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("－过牌!",250,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			Sleep(300);
		}
	}
	return 1;
}
//本机(中间)玩家的出牌处理;
int CSingleGame::CenterDoing()
{
	if( m_nLordLock == 1 )
	{
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;
			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;  //表示当前是地主,上次轮过牌,现在则不能出牌;
	}
	//如果没有选择牌;
	if( pCardsMap->m_nUpCounter == 0 )
	{
		//当不是第一个出牌的时候才可以过牌;
		if( pLeftCards->m_cDiscardingType.m_nTypeNum != 0 || 
			pRightCards->m_cDiscardingType.m_nTypeNum != 0 )
		{
			pDraw->TextGDI("过牌!",470,400,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			Sleep(500);
			if( m_nRoundCounter == 3 )  //说明已经出了牌;
			{
				//把已经出的牌回退;
				if(pCenterCards->m_nDiscardingCounter != 0)
					pDrawCards->CenterBacking();

				if(pRightCards->m_nDiscardingCounter != 0)
					pDrawCards->RightBacking();

				if(pLeftCards->m_nDiscardingCounter != 0)
					pDrawCards->LeftBacking();

				m_nRoundCounter = 0;   //新的一圈;
			}
			m_nRoundCounter++;
			pCenterCards->m_cDiscardingType.m_nTypeNum = 0; //将出的牌值设为空;

			if( m_nCurrentLord == 1 )
			{
				m_nLordLock = 1;
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
	pCenterCards->m_nChoosingCardsCounter = 0;
	//将要出的牌加入到m_cDiscarding[20]数组中,m_nDiscardingCounter计数增加;
	for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
	{
		if(pCardsMap->Center0[i].m_nY== Center_y0 - Card_Up)
		{			
			pCenterCards->m_cChoosingCards[pCenterCards->m_nChoosingCardsCounter] = pCenterCards->m_cCards[i];
			pCenterCards->m_nChoosingCardsCounter++;
		}
	}

	//判断出牌的合法性,
	if( pCenterCards->CheckChoosing() )
	{
		//大小判断;
		
		if( CompareCards() == 0 )
		{
			for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
			{
				if(pCardsMap->Center0[j].m_nY== Center_y0 - Card_Up)
				{			
					pCardsMap->Center0[j].m_nY+= Card_Up;	
				}
			}		
			pCardsMap->m_nUpCounter = 0;  //提起牌数为0;
			::RedrawGame();
			m_bButton = 0;   //延时锁;
			Sleep(400);
			m_bButton = 1;   //延时锁;
			return 0;
		}

		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;
			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}	
		m_nRoundCounter++;

		//将要出的牌加入到m_cDiscarding[20]数组中,m_nDiscardingCounter计数增加;
		for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Center0[i].m_nY== Center_y0 - Card_Up)
			{			
				pCenterCards->m_cDiscarding[pCenterCards->m_nDiscardingCounter] = pCenterCards->m_cCards[i];
				pCenterCards->m_nDiscardingCounter++;
			}
		}	

		if( pCenterCards->m_cDiscardingType.m_nTypeNum == 4 )
		{
			m_nBombCounter++;//加分;	
		}

		pDrawCards->CenterDiscarding();      //本机玩家出牌;

		if( pCenterCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
			pDrawCards->m_bVisual = TRUE;
			AccountScore(1);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 2;
		}
	}
	else
	{
		for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
		{
			if(pCardsMap->Center0[j].m_nY== Center_y0 - Card_Up)
			{			
				pCardsMap->Center0[j].m_nY+= Card_Up;	
			}
		}
		pCardsMap->m_nUpCounter = 0;  //提起牌数为0;
		::RedrawGame();
		m_bButton = 0;   //延时锁;
		Sleep(400);
		m_bButton = 1;   //延时锁;
		return 0;
	}
	return 1;
}
//右边电脑思考，并决定要出的牌;
int CSingleGame::RightDoing()	
{
	if( m_nRoundCounter == 3 )  //说明已经出了牌;
	{
		//把已经出的牌回退;	
		if(pRightCards->m_nDiscardingCounter != 0)
			pDrawCards->RightBacking();

		if(pCenterCards->m_nDiscardingCounter != 0)
			pDrawCards->CenterBacking();

		if(pLeftCards->m_nDiscardingCounter != 0)
			pDrawCards->LeftBacking();

		m_nRoundCounter = 0;   //新的一圈;
	}

	m_nRoundCounter++;

	if( m_nLordLock == 2 )
	{
		return -1;
	}

	if(pCenterCards->m_cDiscardingType.m_nTypeNum == 0 &&
		pLeftCards->m_cDiscardingType.m_nTypeNum == 0)
	{
		m_nLordLock = -1;
		//开始新的一回合,选择一种牌型出;
		pRightCards->DirectChoose();

		if( pRightCards->m_cDiscardingType.m_nTypeNum == 4 )
		{
			m_nBombCounter++;//加分;
		}
		pRightCards->CleanUp();
		pDrawCards->RightDiscarding();
		::RedrawGame();
		if( pRightCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			if( m_nCurrentLord == 0 )
			{
				PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
			}
			else
			{
				PlaySound(MAKEINTRESOURCE(IDR_LOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
			}
			pDrawCards->m_bVisual = TRUE;
			AccountScore(2);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 0;
		}			
		return 1;
	}

	//1.两者都不能大牌,我是最大;
	//2.新的一回合开始,我先出;
		
	if(pCenterCards->m_cDiscardingType.m_nTypeNum != 0)
	{
		//如果上家出了牌,则根据上家决定出牌;
		if( m_nCurrentLord == 0 && 
			m_nLordLock == 0 &&
			pCenterCards->m_cDiscardingType.m_nTypeValue > 13 )
		{
			//不要;
			pRightCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",600,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			Sleep(300);
			return 1;
		}

		//如果上家出了牌,则根据上家决定出牌;
		if( pRightCards->CompareChoose(pCenterCards->m_cDiscardingType) )
		{
			if( pRightCards->m_cDiscardingType.m_nTypeNum == 4 )
			{
				m_nBombCounter++;//加分;
			}
			pRightCards->CleanUp();
			pDrawCards->RightDiscarding();
			::RedrawGame();
			if( pRightCards->m_nCardsCounter == 0 )
			{
				//打印结束信息;
				if(m_nCurrentLord == 0 )
				{
					PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				else
				{
					PlaySound(MAKEINTRESOURCE(IDR_LOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				pDrawCards->m_bVisual = TRUE;
				AccountScore(2);
				::SetTimer(hWnd,0,0,NULL);   //开始游戏;
				return 0;
			}
		}
		else
		{
			//不要;
			pRightCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",600,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			if( m_nCurrentLord == 2 )
			{
				m_nLordLock = 2;
			}
			Sleep(300);
		}
	}
	else
	{
		//如果上家出了牌,则根据上家决定出牌;
		if( m_nCurrentLord == 1 && 
			m_nLordLock == 1 &&
			pLeftCards->m_cDiscardingType.m_nTypeValue > 13 )
		{
			//不要;
			pRightCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",600,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			//补丁:地主一旦过牌,本轮内都不能再出牌;
			Sleep(300);
			return 1;
		}

		//如果上家没出牌,则根据上上家决定出牌;
		if( pRightCards->CompareChoose(pLeftCards->m_cDiscardingType) )
		{
			if( pRightCards->m_cDiscardingType.m_nTypeNum == 4 )
			{
				m_nBombCounter++;//加分;
			}
			pRightCards->CleanUp();
			pDrawCards->RightDiscarding();
			::RedrawGame();
			if( pRightCards->m_nCardsCounter == 0 )
			{
				//打印结束信息;
				if(m_nCurrentLord == 0 )
				{
					PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
				else
				{
					PlaySound(MAKEINTRESOURCE(IDR_LOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
				}
 			    pDrawCards->m_bVisual = TRUE;
				AccountScore(2);
				::SetTimer(hWnd,0,0,NULL);   //开始游戏;
				return 0;
			}
		}
		else
		{
			//不要;
			pRightCards->m_cDiscardingType.m_nTypeNum = 0;
			pDraw->TextGDI("过牌!－",600,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			if( m_nCurrentLord == 2 )
			{
				m_nLordLock = 2;
			}
			Sleep(300);
		}
	}
	return 1;
}
