//Download by http://www.NewXing.com
// Draw_Cards_Engine.cpp: implementation of the CDrawCardsEngine class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "斗地主.h"

#include "MainFrm.h"

#include "Game.h"

#include "My_DirectInput.h"
#include "my_directdraw.h"

#include "Draw_Cards_Engine.h"
#include "Playing_Cards.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//扑克的位置;
CCardPos::CCardPos()
{
	m_nX      = -1;       //表示没有;
	m_nY      = -1;
	m_bVisual = 1;   //表示可见;
}
//扑克牌布局地图结构初始化;
CCardsMap::CCardsMap()
{
	for(int i=0;i<20;i++)
	{
		Center0[i].m_nX = Center_x0 + Card_Distance1 * i;
		Center0[i].m_nY = Center_y0;

		Center1[i].m_nX = Center_x1 + Card_Distance0 * i;
		Center1[i].m_nY = Center_y1;

		Center2[i].m_nX = Center_x2 + Card_Distance2 * i;
		Center2[i].m_nY = Center_y2;
	}

	for(int j=0;j<20;j++)
	{
		Left0[j].m_nX = Left_x0; 
		Left0[j].m_nY = Left_y0 + Card_Distance0 * j;

		Left1[j].m_nX = Left_x1;
		Left1[j].m_nY = Left_y1 + Card_Distance1 * j;
		
		Left2[j].m_nX = Left_x2;
		Left2[j].m_nY = Left_y2 + Card_Distance2 * j;
	}

	for(int k=0;k<20;k++)
	{
		Right0[k].m_nX = Right_x0;
		Right0[k].m_nY = Right_y0 + Card_Distance0 * k;

		Right1[k].m_nX = Right_x1;
		Right1[k].m_nY = Right_y1 + Card_Distance1 * k;

		Right2[k].m_nX = Right_x2;
		Right2[k].m_nY = Right_y2 + Card_Distance2 * k;
	}

	m_cLordCards[0].m_nX = Lord_Card1_x;
	m_cLordCards[0].m_nY = Lord_Card1_y;

	m_cLordCards[1].m_nX = Lord_Card2_x;
	m_cLordCards[1].m_nY = Lord_Card2_y;

	m_cLordCards[2].m_nX = Lord_Card3_x;
	m_cLordCards[2].m_nY = Lord_Card3_y;

	m_cDistributeCard.m_nX = Card_x; 
	m_cDistributeCard.m_nY = Card_y; 

	m_nLordCards = 0;
	m_nUpCounter = 0;
}

//CDrawCardsEngine
CDrawCardsEngine::CDrawCardsEngine():BLUE( _RGB32BIT(0,0,0,255) )
{
	m_bVisual = FALSE;
}

CDrawCardsEngine::~CDrawCardsEngine()
{

}
//渲染初始化;
int CDrawCardsEngine::Init()
{
	char* file;
//14种牌从小到大的加载;
//3-10
	char filename[50];
	for(int i=0;i<=7;i++)
	{
		sprintf(filename,"bmp\\Cards\\%d.bmp",i+3);
		lpddsbg_Cards[i] = pDraw->CreateSurface(Cards_Width*4,Cards_Height,0,BLUE);
		pDraw->BitmapToSurface(lpddsbg_Cards[i],0,0,filename,Cards_Width*4,Cards_Height);
	}

//J
	file = "bmp\\Cards\\j.bmp";
	lpddsbg_Cards[8] = pDraw->CreateSurface(Cards_Width*4,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[8],0,0,file,Cards_Width*4,Cards_Height);
//Q
	file = "bmp\\Cards\\Q.bmp";
	lpddsbg_Cards[9] = pDraw->CreateSurface(Cards_Width*4,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[9],0,0,file,Cards_Width*4,Cards_Height);
//K
	file = "bmp\\Cards\\K.bmp";
	lpddsbg_Cards[10] = pDraw->CreateSurface(Cards_Width*4,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[10],0,0,file,Cards_Width*4,Cards_Height);
//A
	file = "bmp\\Cards\\A.bmp";
	lpddsbg_Cards[11] = pDraw->CreateSurface(Cards_Width*4,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[11],0,0,file,Cards_Width*4,Cards_Height);
//2
	file = "bmp\\Cards\\2.bmp";
	lpddsbg_Cards[12] = pDraw->CreateSurface(Cards_Width*4,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[12],0,0,file,Cards_Width*4,Cards_Height);
//加载百搭;
	file = "bmp\\Cards\\jokers.bmp";
	lpddsbg_Cards[13] = pDraw->CreateSurface(Cards_Width*2,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[13],0,0,file,Cards_Width*2,Cards_Height);
//加载扑克背面;
	file = "bmp\\Cards\\card_back1.bmp";
	lpddsbg_Cards[14] = pDraw->CreateSurface(Cards_Width,Cards_Height,0,BLUE);
	pDraw->BitmapToSurface(lpddsbg_Cards[14],0,0,file,Cards_Width,Cards_Height);

	return 1;
}

int CDrawCardsEngine::DrawCard(int x,
									int y,
									int Card_m_nColor,
									int Card_Value,
									LPDIRECTDRAWSURFACE7 lpdds_Dest ) 
{

	lpdds_Dest->Blt(CRect(x,
						  y,
						  x+Cards_Width,
						  y+Cards_Height),
				    lpddsbg_Cards[Card_Value - 3],
				    CRect(Card_m_nColor*Cards_Width,
						  0,
						  Card_m_nColor*Cards_Width + Cards_Width,
						  Cards_Height),
				    DDBLT_WAIT | DDBLT_KEYSRC,
				    NULL);
	return 1;
}

int CDrawCardsEngine::MoveCard(int Dest_x,
								    int Dest_y,
									int Source_x,
									int Source_y,
									int Card_m_nColor,
									int Card_Value,
									int Speed)		//移动速度,以象素为单位;	
//									LPDIRECTDRAWSURFACE7 lpdds_Dest = lpddsback)
{
	int dx,dy,dx2,dy2,x_inc,y_inc,error;

	int x = Source_x,y = Source_y;

	dx = Dest_x - Source_x;

	dy = Dest_y - Source_y;
	
	if(dx>=0)
	{
		x_inc = 1;
	}
	else
	{
		x_inc = -1;
		dx = -dx;
	}

	if(dy >= 0)
	{
		y_inc = 1;
	}
	else
	{
		y_inc = -1;
		dy = -dy;
	}

	dx2 = dx << 1;
	dy2 = dy << 1;

	if(dx > dy)
	{
		error = dy2 - dx;
		for(int i=0;i<dx;i++)
		{
			if(i%Speed == 0)
			{
				::RedrawGame(NULL);

				DrawCard(x,y,Card_m_nColor,Card_Value);

				while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));
			}

			if(error >= 0)
			{
				error-=dx2;
				y+=y_inc;
			}
			error+=dy2;
			x+=x_inc;
		}//end for~
	}
	else
	{
		error = dx2 - dy;
		for(int i=0;i<dy;i++)
		{
			if(i%Speed == 0)
			{
				::RedrawGame(NULL);

				DrawCard(x,y,Card_m_nColor,Card_Value);

				while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));
			}

			if(error >= 0)
			{
				error-=dy2;
				x+=x_inc;
			}
			error+=dx2;
			y+=y_inc;
		}//end for~

	}
	::RedrawGame(NULL);

	DrawCard(x,y,Card_m_nColor,Card_Value);
	
	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));

	return 1;
}
//重新整理中间(本机)玩家的牌面;
int CDrawCardsEngine::DrawCleanUp()
{
	pCenterCards->CleanUp();	//整理牌;
	for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
	{
		pCardsMap->Center0[i].m_bVisual = 0;
	}
	for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
	{
		pCardsMap->Center0[j].m_bVisual = 1;
		::RedrawGame();	//刷新;
	}
	return 1;
}
//发牌;
int CDrawCardsEngine::Dealing()
{
//分牌给三个玩家;	
	for(int i=0;i<17;i++)
	{
//发牌给左边玩家;
		pLeftCards->m_cCards[i]  = g_cAllCards[i*3 + 0];
		MoveCard(Left_x0,
					pCardsMap->Left0[i].m_nY,
					Card_x,
					Card_y,
					0,
					17);
		
		pLeftCards->m_nCardsCounter++;

//发牌给中间玩家;
		pCenterCards->m_cCards[i] = g_cAllCards[i*3 + 1];
		MoveCard(pCardsMap->Center0[i].m_nX,
					Center_y0,
					Card_x,
					Card_y,
					pCenterCards->m_cCards[i].m_nColor,
					pCenterCards->m_cCards[i].m_nValue,
					100);
		pCenterCards->m_nCardsCounter++;

		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//发牌给右边玩家;
		pRightCards->m_cCards[i] = g_cAllCards[i*3 + 2];  
		MoveCard(Right_x0,
					pCardsMap->Right0[i].m_nY,
					Card_x,
					Card_y,
					0,
					17);
		pRightCards->m_nCardsCounter++;
	}

//测试用牌;
//	int j;
//	for(j=0;j<17;j++)
//	{
//		pCenterCards->m_cCards[j].m_nValue = 3+j/4;
//	}
/*
	pCenterCards->m_cCards[0].m_nValue = 4;
	pCenterCards->m_cCards[1].m_nValue = 4;
	pCenterCards->m_cCards[2].m_nValue = 4;
	pCenterCards->m_cCards[3].m_nValue = 4;
	pCenterCards->m_cCards[4].m_nValue = 8;
	pCenterCards->m_cCards[5].m_nValue = 8;
	pCenterCards->m_cCards[6].m_nValue = 8;
	pCenterCards->m_cCards[7].m_nValue = 8;
	pCenterCards->m_cCards[8].m_nValue = 11;
	pCenterCards->m_cCards[9].m_nValue = 11;
	pCenterCards->m_cCards[10].m_nValue = 11;
	pCenterCards->m_cCards[12].m_nValue = 11;
	pCenterCards->m_cCards[13].m_nValue = 3;
	pCenterCards->m_cCards[14].m_nValue = 3;
	pCenterCards->m_cCards[15].m_nValue = 3;
	pCenterCards->m_cCards[16].m_nValue = 3;
//	for(j=0;j<17;j++)
//	{
//		pRightCards->m_cCards[j].m_nValue = 4+j/4;
//	}
*/
/*
	pRightCards->m_cCards[0].m_nValue = 3;
	pRightCards->m_cCards[1].m_nValue = 3;
	pRightCards->m_cCards[2].m_nValue = 4;

	pRightCards->m_cCards[3].m_nValue = 4;
	pRightCards->m_cCards[4].m_nValue = 5;
	pRightCards->m_cCards[5].m_nValue = 5;
	pRightCards->m_cCards[6].m_nValue = 9;
	pRightCards->m_cCards[7].m_nValue = 9;
	pRightCards->m_cCards[8].m_nValue = 9;

	pRightCards->m_cCards[9].m_nValue = 8;
	pRightCards->m_cCards[10].m_nValue = 11;
	pRightCards->m_cCards[11].m_nValue = 11;
	pRightCards->m_cCards[12].m_nValue = 13;
	pRightCards->m_cCards[13].m_nValue = 13;
	pRightCards->m_cCards[14].m_nValue = 13;

	pRightCards->m_cCards[15].m_nValue = 16;
	pRightCards->m_cCards[15].m_nColor = 0;
	pRightCards->m_cCards[16].m_nValue = 16;
	pRightCards->m_cCards[16].m_nColor = 1;
*/
//	for(j=0;j<17;j++)
//	{
//		pLeftCards->m_cCards[j].m_nValue = 7+j/4;
//	}

//	pLeftCards->m_cCards[0].m_nValue = 3;
//	pLeftCards->m_cCards[1].m_nValue = 3;
//	pLeftCards->m_cCards[12].m_nValue = 4;
//	pLeftCards->m_cCards[13].m_nValue = 4;



//开始向上发地主牌;
//第一张地主牌;
	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
	MoveCard(Lord_Card1_x,
				Lord_Card1_y,
				Card_x,
				Card_y,
				0,
				17,
				50);

	pCardsMap->m_cLordCards[0].m_bVisual = 1;

	::RedrawGame();

//第二张地主牌;
	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
	MoveCard(Lord_Card2_x,
				Lord_Card2_y,
				Card_x,
				Card_y,
				0,
				17,
				50);

	pCardsMap->m_cLordCards[1].m_bVisual = 1;
		
	::RedrawGame();
//第三张地主牌;
	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
	MoveCard(Lord_Card3_x,
				Lord_Card3_y,
				Card_x,
				Card_y,
				0,
				17,
				50);
	pCardsMap->m_cLordCards[2].m_bVisual = 1;
	
	pCardsMap->m_cDistributeCard.m_bVisual = 0;   //让中间发的牌不可见;
	::RedrawGame();

	DrawCleanUp();

//	Sleep(60000);
	return 1;
}
//发地主牌;
int CDrawCardsEngine::DealingLord(int bLord)
{
	pCardsMap->m_nLordCards = 2;
	::RedrawGame();
	if(bLord == 0)
	{
//发地主牌给左边玩家;
//第一张;
		pLeftCards->m_cCards[17]  = g_cAllCards[51];  //第十八张牌;
		MoveCard(Left_x0,
				 pCardsMap->Left0[17].m_nY,
				 Lord_Card1_x,
				 Lord_Card1_y,
				 pLeftCards->m_cCards[17].m_nColor,
				 pLeftCards->m_cCards[17].m_nValue,
				 80);

		pLeftCards->m_nCardsCounter++;  //m_nCardsCounter =18

		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//		::Redraw_Game();
//第二张;
		pLeftCards->m_cCards[18]  = g_cAllCards[52];
		MoveCard(Left_x0,
				 pCardsMap->Left0[18].m_nY,
				 Lord_Card2_x,
				 Lord_Card2_y,
				 pLeftCards->m_cCards[18].m_nColor,
				 pLeftCards->m_cCards[18].m_nValue,
				 80);
		pLeftCards->m_nCardsCounter++; //m_nCardsCounter =19
		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//		::Redraw_Game();

//第三张;
		pLeftCards->m_cCards[19]  = g_cAllCards[53];
		MoveCard(Left_x0,
				 pCardsMap->Left0[19].m_nY,
				 Lord_Card3_x,
				 Lord_Card3_y,
				 pLeftCards->m_cCards[19].m_nColor,
				 pLeftCards->m_cCards[19].m_nValue,
				 80);
		pLeftCards->m_nCardsCounter++;  //m_nCardsCounter =20

		pLeftCards->m_nCardsTable[pLeftCards->m_cCards[17].m_nValue]++;
		pLeftCards->m_nCardsTable[pLeftCards->m_cCards[18].m_nValue]++;
		pLeftCards->m_nCardsTable[pLeftCards->m_cCards[19].m_nValue]++;

		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
		::RedrawGame();
	}
	else if(bLord == 1)
	{
//发地主牌给中间玩家;
//第一张;
		pCenterCards->m_cCards[17]  = g_cAllCards[51];  //第十八张牌;
		MoveCard(pCardsMap->Center0[17].m_nX,
				 Center_y0,
				 Lord_Card1_x,
				 Lord_Card1_y,
				 pCenterCards->m_cCards[17].m_nColor,
				 pCenterCards->m_cCards[17].m_nValue,
				 80);
		pCenterCards->m_nCardsCounter++;  //m_nCardsCounter =18
		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//		::Redraw_Game();
//第二张;

		pCenterCards->m_cCards[18]  = g_cAllCards[52];
		MoveCard(pCardsMap->Center0[18].m_nX,
				 Center_y0,
				 Lord_Card2_x,
				 Lord_Card2_y,
				 pCenterCards->m_cCards[18].m_nColor,
				 pCenterCards->m_cCards[18].m_nValue,
				 80);
		pCenterCards->m_nCardsCounter++; //m_nCardsCounter =19
		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//		::Redraw_Game();

//第三张;
		pCenterCards->m_cCards[19]  = g_cAllCards[53];
		MoveCard(pCardsMap->Center0[19].m_nX,
				 Center_y0,
				 Lord_Card3_x,
				 Lord_Card3_y,
				 pCenterCards->m_cCards[19].m_nColor,
				 pCenterCards->m_cCards[19].m_nValue,
				 80);
		pCenterCards->m_nCardsCounter++;  //m_nCardsCounter =20
		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
		::RedrawGame();
//拿到地主牌后再整理一次;
		DrawCleanUp();
	}
	else if(bLord == 2)
	{
//发地主牌给右边玩家;
//第一张;
		pRightCards->m_cCards[17]  = g_cAllCards[51];  //第十八张牌;
		MoveCard(Right_x0,
			 	 pCardsMap->Right0[17].m_nY,
				 Lord_Card1_x,
				 Lord_Card1_y,
				 pRightCards->m_cCards[17].m_nColor,
				 pRightCards->m_cCards[17].m_nValue,
				 80);
		pRightCards->m_nCardsCounter++;  //m_nCardsCounter =18
		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//		::Redraw_Game();
//第二张;

		pRightCards->m_cCards[18]  = g_cAllCards[52];
		MoveCard(Right_x0,
				 pCardsMap->Right0[18].m_nY,
				 Lord_Card2_x,
			 	 Lord_Card2_y,
				 pRightCards->m_cCards[18].m_nColor,
				 pRightCards->m_cCards[18].m_nValue,
				 80);
		pRightCards->m_nCardsCounter++; //m_nCardsCounter =19
		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
//		::Redraw_Game();

//第三张;
		pRightCards->m_cCards[19] = g_cAllCards[53];
		MoveCard(Right_x0,
				 pCardsMap->Right0[19].m_nY,
				 Lord_Card3_x,
				 Lord_Card3_y,
				 pRightCards->m_cCards[19].m_nColor,
				 pRightCards->m_cCards[19].m_nValue,
				 80);
		pRightCards->m_nCardsCounter++;  //m_nCardsCounter =20

		pRightCards->m_nCardsTable[pRightCards->m_cCards[17].m_nValue]++;
		pRightCards->m_nCardsTable[pRightCards->m_cCards[18].m_nValue]++;
		pRightCards->m_nCardsTable[pRightCards->m_cCards[19].m_nValue]++;

		PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
			SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
		
		::RedrawGame();
	}
	pLeftCards->CleanUp();
	pRightCards->CleanUp();
	::RedrawGame();
	return 1;
}
//中间(本机)玩家出牌;
int CDrawCardsEngine::CenterDiscarding()
{
	///检查出牌的合法性,调用CPlaying_Cards的方法;

	int counter = 0;

	pCenterCards->m_nDiscardingCounter = 0;

	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //

	for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
	{
		if(pCardsMap->Center0[i].m_nY == Center_y0 - Card_Up)
		{
			//移动提起的牌到刚出牌区;
			pCardsMap->Center0[i].m_bVisual = 0;

			MoveCard(pCardsMap->Center2[pCenterCards->m_nDiscardingCounter].m_nX,
						pCardsMap->Center2[pCenterCards->m_nDiscardingCounter].m_nY,
						pCardsMap->Center0[i].m_nX,
						pCardsMap->Center0[i].m_nY,
					    pCenterCards->m_cCards[i].m_nColor,
						pCenterCards->m_cCards[i].m_nValue,
						80);

			pCardsMap->Center0[i].m_nY += Card_Up;
			
			pCenterCards->m_cCards[i].m_nColor = 4;   //花色为4表示已经出过牌;
			pCenterCards->m_cCards[i].m_nValue = 17;   //值为17表示已经出过牌;
			counter++;
			pCenterCards->m_nDiscardingCounter++;
		}
	}
	//将中间(本机)的牌置为可见;
	for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
	{
		pCardsMap->Center0[j].m_bVisual = 1;
	}
	DrawCleanUp();
	pCenterCards->m_nCardsCounter -= counter;
//	pCenterCards->m_nDiscardingCounter = 0;

	pCardsMap->m_nUpCounter = 0;  //提起牌数为0;

	return 1;
}
//本机玩家刚出的牌从刚出牌区移动到已经出牌区;
int CDrawCardsEngine::CenterBacking()
{
	int temp_counter = pCenterCards->m_nDiscardingCounter ;

	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
					SND_RESOURCE|SND_NODEFAULT );  //
	
	for(int i=0;i<temp_counter;i++)
	{
		//移动提起的牌到刚出牌区;

		MoveCard(pCardsMap->Center1[pCenterCards->m_nDiscardedCounter].m_nX,
					pCardsMap->Center1[pCenterCards->m_nDiscardedCounter].m_nY,
					pCardsMap->Center2[i].m_nX,
					pCardsMap->Center2[i].m_nY,
					pCenterCards->m_cDiscarding[i].m_nColor,
					pCenterCards->m_cDiscarding[i].m_nValue,
					100);

		pCenterCards->m_nDiscardingCounter--;
				
		pCenterCards->m_cDiscarded[pCenterCards->m_nDiscardedCounter] = pCenterCards->m_cDiscarding[i];
		pCenterCards->m_nDiscardedCounter++;
		
	}
	::RedrawGame();
	return 1;
}
//左边玩家出牌;
int CDrawCardsEngine::LeftDiscarding()	 
{
	int temp_counter = pLeftCards->m_nDiscardingCounter;

	pLeftCards->m_nDiscardingCounter = 0;

	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //

	for(int i=0;i<temp_counter;i++)
	{
	
		//移动提起的牌到刚出牌区;
		MoveCard(pCardsMap->Left2[i].m_nX,
					pCardsMap->Left2[i].m_nY,
					pCardsMap->Left0[pLeftCards->m_nCardsCounter - 1].m_nX,
					pCardsMap->Left0[pLeftCards->m_nCardsCounter - 1].m_nY,
					pLeftCards->m_cDiscarding[i].m_nColor,
					pLeftCards->m_cDiscarding[i].m_nValue,
					100);

		pLeftCards->m_nDiscardingCounter++;
		pLeftCards->m_nCardsCounter--;
	}
//	::Redraw_Game();
	return 1;
}
//左边玩家刚出的牌从刚出牌区移动到已经出牌区;
int CDrawCardsEngine::LeftBacking()
{
	int temp_counter = pLeftCards->m_nDiscardingCounter ;

	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
	
	for(int i=0;i<temp_counter;i++)
	{

		//移动提起的牌到刚出牌区;
		
		MoveCard(pCardsMap->Left1[pLeftCards->m_nDiscardedCounter].m_nX,
					pCardsMap->Left1[pLeftCards->m_nDiscardedCounter].m_nY,
					pCardsMap->Left2[i].m_nX,
					pCardsMap->Left2[i].m_nY,
					pLeftCards->m_cDiscarding[i].m_nColor,
					pLeftCards->m_cDiscarding[i].m_nValue,
					140);

		pLeftCards->m_nDiscardingCounter--;
				
		pLeftCards->m_cDiscarded[pLeftCards->m_nDiscardedCounter] = pLeftCards->m_cDiscarding[i];
		pLeftCards->m_nDiscardedCounter++;
		
	}
	::RedrawGame();
	return 1;
}	
//右边玩家出牌;
int CDrawCardsEngine::RightDiscarding()
{
	int temp_counter = pRightCards->m_nDiscardingCounter;

	pRightCards->m_nDiscardingCounter = 0;

	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
	for(int i=0;i<temp_counter;i++)
	{
	
		//移动提起的牌到刚出牌区;
		MoveCard(pCardsMap->Right2[i].m_nX,
					pCardsMap->Right2[i].m_nY,
					pCardsMap->Right0[pRightCards->m_nCardsCounter - 1].m_nX,
					pCardsMap->Right0[pRightCards->m_nCardsCounter - 1].m_nY,
					pRightCards->m_cDiscarding[i].m_nColor,
					pRightCards->m_cDiscarding[i].m_nValue,
					100);

		pRightCards->m_nDiscardingCounter++;
		pRightCards->m_nCardsCounter--;
	}
	return 1;
}
//右边玩家刚出的牌从刚出牌区移动到已经出牌区;
int CDrawCardsEngine::RightBacking()
{
	int temp_counter = pRightCards->m_nDiscardingCounter ;

	PlaySound(MAKEINTRESOURCE(IDR_GIVE),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	

	for(int i=0;i<temp_counter;i++)
	{

		//移动提起的牌到刚出牌区;
		
		MoveCard(pCardsMap->Right1[pLeftCards->m_nDiscardedCounter].m_nX,
					pCardsMap->Right1[pLeftCards->m_nDiscardedCounter].m_nY,
					pCardsMap->Right2[i].m_nX,
					pCardsMap->Right2[i].m_nY,
					pRightCards->m_cDiscarding[i].m_nColor,
					pRightCards->m_cDiscarding[i].m_nValue,
					140);

		pRightCards->m_nDiscardingCounter--;
				
		pRightCards->m_cDiscarded[pRightCards->m_nDiscardedCounter] = pRightCards->m_cDiscarding[i];
		pRightCards->m_nDiscardedCounter++;
		
	}
	::RedrawGame();
	return 1;
}
//重画;
void CDrawCardsEngine::Redraw(LPDIRECTDRAWSURFACE7  pSurface)
{
//左边未出的牌;
	int i;
	if( m_bVisual )
	{
		for(i=0;i<pLeftCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Left0[i].m_bVisual == 1)
			{
				DrawCard(pCardsMap->Left0[i].m_nX,
						 pCardsMap->Left0[i].m_nY,
						 pLeftCards->m_cCards[i].m_nColor,	
						 pLeftCards->m_cCards[i].m_nValue);	
			}
		}
	}
	else//
	{
		for(i=0;i<pLeftCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Left0[i].m_bVisual == 1)
			{
				DrawCard(pCardsMap->Left0[i].m_nX,
						 pCardsMap->Left0[i].m_nY,
						 0,	
						 17);	
			}
		}
	}
//左边已经出的牌;
	for(i=0;i<pLeftCards->m_nDiscardedCounter;i++)
	{
		if(pCardsMap->Left1[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Left1[i].m_nX,
					 pCardsMap->Left1[i].m_nY,
					 pLeftCards->m_cDiscarded[i].m_nColor,
					 pLeftCards->m_cDiscarded[i].m_nValue);
		}
	}
//左边刚出的牌;
	for(i=0;i<pLeftCards->m_nDiscardingCounter;i++)
	{
		if(pCardsMap->Left2[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Left2[i].m_nX,
					 pCardsMap->Left2[i].m_nY,
					 pLeftCards->m_cDiscarding[i].m_nColor,
					 pLeftCards->m_cDiscarding[i].m_nValue);
		}
	}
//中间未出的牌;
	for(i=0;i<pCenterCards->m_nCardsCounter;i++)
	{
		if(pCardsMap->Center0[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Center0[i].m_nX,
					pCardsMap->Center0[i].m_nY,
					pCenterCards->m_cCards[i].m_nColor,
					pCenterCards->m_cCards[i].m_nValue);
		}
	}
//中间已经出的牌;
	for(i=0;i<pCenterCards->m_nDiscardedCounter;i++)
	{
		if(pCardsMap->Center1[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Center1[i].m_nX,
					 pCardsMap->Center1[i].m_nY,
					 pCenterCards->m_cDiscarded[i].m_nColor,
					 pCenterCards->m_cDiscarded[i].m_nValue);
		}
	}
//中间刚出的牌;
	for(i=0;i<pCenterCards->m_nDiscardingCounter;i++)
	{
		if(pCardsMap->Center2[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Center2[i].m_nX,
					 pCardsMap->Center2[i].m_nY,
					 pCenterCards->m_cDiscarding[i].m_nColor,
					 pCenterCards->m_cDiscarding[i].m_nValue);
		}
	}
//右边未出的牌;
	if( m_bVisual )
	{
		for(i=0;i<pRightCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Right0[i].m_bVisual == 1)
			{
				DrawCard(pCardsMap->Right0[i].m_nX,
						 pCardsMap->Right0[i].m_nY,
						 pRightCards->m_cCards[i].m_nColor,	
						 pRightCards->m_cCards[i].m_nValue);
			}
		}
	}
	else
	{
		for(i=0;i<pRightCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Right0[i].m_bVisual == 1)
			{
				DrawCard(pCardsMap->Right0[i].m_nX,
						 pCardsMap->Right0[i].m_nY,
						 0,
						 17);
			}
		}
	}
//右边已经出的牌;
	for(i=0;i<pRightCards->m_nDiscardedCounter;i++)
	{
		if(pCardsMap->Right1[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Right1[i].m_nX,
					 pCardsMap->Right1[i].m_nY,
					 pRightCards->m_cDiscarded[i].m_nColor,
					 pRightCards->m_cDiscarded[i].m_nValue);
		}
	}
//右边刚出的牌;
	for(i=0;i<pRightCards->m_nDiscardingCounter;i++)
	{
		if(pCardsMap->Right2[i].m_bVisual == 1)
		{
			DrawCard(pCardsMap->Right2[i].m_nX,
					 pCardsMap->Right2[i].m_nY,
					 pRightCards->m_cDiscarding[i].m_nColor,
					 pRightCards->m_cDiscarding[i].m_nValue);
		}
	}
//正在发的牌;

	if(pCardsMap->m_cDistributeCard.m_bVisual == 1)
	{
		DrawCard(pCardsMap->m_cDistributeCard.m_nX,
				 pCardsMap->m_cDistributeCard.m_nY,
				 0,
				 17);
	}
//陈列的三张地主牌;
	if(pCardsMap->m_nLordCards == 1)
	{
		for(i=0;i<3;i++)
		{
			if(pCardsMap->m_cLordCards[i].m_bVisual == 1)
			{
				DrawCard(pCardsMap->m_cLordCards[i].m_nX,
						 pCardsMap->m_cLordCards[i].m_nY,
					     0,
						 17);
			}
		}//end for
	}
	else if(pCardsMap->m_nLordCards == 2)
	{
		for(i=0;i<3;i++)
		{
			if(pCardsMap->m_cLordCards[i].m_bVisual == 1)
			{
				DrawCard(pCardsMap->m_cLordCards[i].m_nX,
						 pCardsMap->m_cLordCards[i].m_nY,
						 ::g_cAllCards[51+i].m_nColor,
						 ::g_cAllCards[51+i].m_nValue);
			}
		}//end for
	}
}	