//Download by http://www.NewXing.com
#if !defined(AFX_DLG_START_H__229FB0DE_EB29_4305_ADC1_F4E3252391A0__INCLUDED_)
#define AFX_DLG_START_H__229FB0DE_EB29_4305_ADC1_F4E3252391A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_Start.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlg_Start dialog

class CDlg_Start : public CDialog
{
public:
	CBitmap  m_cBitmap0;
	CBitmap  m_cBitmap1;
	CBitmap  m_cBitmap2;
	CBitmap  m_cBitmap3;

	static CString s_m_szNetInfo;  //��̬����;
	static int s_m_bOK;

	HANDLE m_hThread;
	DWORD  m_dwThreadId;	
// Construction
public:
	CDlg_Start(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CDlg_Start)
	enum { IDD = IDD_START };
	CString	m_ServerIP;
	CString	m_Client;
	CString	m_Name;
	int		m_Face_ID;
	CString	m_Net_States;
	CString	m_Net_Info;
	//}}AFX_DATA

	static DWORD WINAPI Server_Login(LPVOID pParam);
	static DWORD WINAPI Client_Login(LPVOID pParam);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlg_Start)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlg_Start)
	afx_msg void OnExit();
	virtual BOOL OnInitDialog();
	afx_msg void OnClient();
	afx_msg void OnLink();
	afx_msg void OnSingle();
	afx_msg void OnServer();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnStart();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnGameMaker();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_START_H__229FB0DE_EB29_4305_ADC1_F4E3252391A0__INCLUDED_)
