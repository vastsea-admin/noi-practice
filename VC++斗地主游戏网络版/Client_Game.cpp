//Download by http://www.NewXing.com
// Client_Game.cpp: implementation of the CClientGame class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "斗地主.h"

#include "Game.h"

#include "Link.h"
#include "My_DirectInput.h"
#include "my_directdraw.h"

#include "Playing_Cards.h"


#include "Draw_Item_Engine.h"
#include "Draw_Cards_Engine.h"

#include "Client_Game.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CClientGame::CClientGame()
{

}

CClientGame::~CClientGame()
{

}
//左边玩家（电脑）思考，并决定要出的牌;
int CClientGame::LeftDoing()
{	
	if( m_nLordLock == 0 )
	{		
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;
	}

	if( pRightCards->m_cDiscardingType.m_nTypeNum == 0 && 
		pCenterCards->m_cDiscardingType.m_nTypeNum == 0 )
	{
		m_nLordLock = -1;
	}
	
	//补丁,调整出牌显示;
	int Temp_Discarding_Counter = 0;  //刚出的牌的数量;
	CCard Temp_Discarding[20];         //刚出的牌;

	pClientLink->Recv(Temp_Discarding,sizeof(CCard)*20);
	pClientLink->Recv(&Temp_Discarding_Counter,sizeof(int));
	pClientLink->Recv(&pLeftCards->m_cDiscardingType,sizeof(CCardsType));	

	if( m_nRoundCounter == 3 )  //说明已经出了牌;
	{
		//把已经出的牌回退;
		if(pLeftCards->m_nDiscardingCounter != 0)
			pDrawCards->LeftBacking();

		if(pCenterCards->m_nDiscardingCounter != 0)
			pDrawCards->CenterBacking();

		if(pRightCards->m_nDiscardingCounter != 0)
			pDrawCards->RightBacking();

		m_nRoundCounter = 0;   //新的一圈;
	}
	m_nRoundCounter++;

	for(int i=0;i<20;i++)
	{
		pLeftCards->m_cDiscarding[i] = Temp_Discarding[i];
	}
	pLeftCards->m_nDiscardingCounter = Temp_Discarding_Counter;

	//如果出牌数大于0;
	if( pLeftCards->m_cDiscardingType.m_nTypeNum == 4 )
	{
		m_nBombCounter++;//加分;
	}

	if( pLeftCards->m_cDiscardingType.m_nTypeNum != 0 )
	{
		pDrawCards->LeftDiscarding();
		::RedrawGame();
		if( pLeftCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			AccountScore(0);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 0;
		}
	}
	else
	{
		//打印左边玩家不要信息;
		pDraw->TextGDI("过牌!－",250,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
		if( m_nCurrentLord == 0 )
		{
			m_nLordLock = 0;
		}
		Sleep(300);
	}
	return 1;
}
//中间(本机)玩家出牌;
int CClientGame::CenterDoing()
{	
	if( m_nLordLock == 1 )
	{
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;
			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;  //表示当前是地主,上次轮过牌,现在则不能出牌;
	}

	if( pLeftCards->m_cDiscardingType.m_nTypeNum == 0 &&
		pRightCards->m_cDiscardingType.m_nTypeNum == 0 )
	{
		m_nLordLock = -1;
	}

	//如果没有选择牌;
	if( pCardsMap->m_nUpCounter == 0 )
	{
		//当不是第一个出牌的时候才可以过牌;
		if( pLeftCards->m_cDiscardingType.m_nTypeNum != 0 || 
			pRightCards->m_cDiscardingType.m_nTypeNum != 0 )
		{
			pDraw->TextGDI("过牌!",470,400,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			Sleep(300);

			if( m_nRoundCounter == 3 )  //说明已经出了牌;
			{
				//把已经出的牌回退;
				if(pCenterCards->m_nDiscardingCounter != 0)
					pDrawCards->CenterBacking();

				if(pRightCards->m_nDiscardingCounter != 0)
					pDrawCards->RightBacking();

				if(pLeftCards->m_nDiscardingCounter != 0)
					pDrawCards->LeftBacking();

				m_nRoundCounter = 0;   //新的一圈;
			}

			m_nRoundCounter++;
			if( m_nCurrentLord == 1 )
			{
				m_nLordLock = 1;
			}

			pCenterCards->m_cDiscardingType.m_nTypeNum = 0;

			pClientLink->Send(pCenterCards->m_cDiscarding,sizeof(CCard)*20);
			pClientLink->Send(&pCenterCards->m_nDiscardingCounter,sizeof(int));
			pClientLink->Send(&pCenterCards->m_cDiscardingType,sizeof(CCardsType));	

			return 1;
		}
		else
		{
			return 0;
		}
	}

	pCenterCards->m_nChoosingCardsCounter = 0;
	//将要出的牌加入到m_cDiscarding[20]数组中,m_nDiscardingCounter计数增加;
	for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
	{
		if(pCardsMap->Center0[i].m_nY == Center_y0 - Card_Up)
		{			
			pCenterCards->m_cChoosingCards[pCenterCards->m_nChoosingCardsCounter] = pCenterCards->m_cCards[i];
			pCenterCards->m_nChoosingCardsCounter++;
		}
	}

	//判断出牌的合法性,
	if( pCenterCards->CheckChoosing() )
	{
		//大小判断;
		if( CompareCards() == 0 )
		{
			for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
			{
				if(pCardsMap->Center0[j].m_nY == Center_y0 - Card_Up)
				{			
					pCardsMap->Center0[j].m_nY += Card_Up;	
				}
			}		
			pCardsMap->m_nUpCounter = 0;  //提起牌数为0;
			::RedrawGame();
			m_bButton = 0;   //延时锁;
			Sleep(400);
			m_bButton = 1;   //延时锁;
			return 0;
		}

		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;
			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}	
		m_nRoundCounter++;
		//将要出的牌加入到m_cDiscarding[20]数组中,m_nDiscardingCounter计数增加;
		for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Center0[i].m_nY == Center_y0 - Card_Up)
			{			
				pCenterCards->m_cDiscarding[pCenterCards->m_nDiscardingCounter] = pCenterCards->m_cCards[i];
				pCenterCards->m_nDiscardingCounter++;
			}
		}
		//发送给其他两个玩家;
		pClientLink->Send(pCenterCards->m_cDiscarding,sizeof(CCard)*20);
		pClientLink->Send(&pCenterCards->m_nDiscardingCounter,sizeof(int));
		pClientLink->Send(&pCenterCards->m_cDiscardingType,sizeof(CCardsType));	

		if( pCenterCards->m_cDiscardingType.m_nTypeNum == 4 )
		{
			m_nBombCounter++;//加分;
		}

		pDrawCards->CenterDiscarding();      //本机玩家出牌;

		if( pCenterCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
			AccountScore(1);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 2;
		}
		return 1;
	}
	else
	{
		for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
		{
			if(pCardsMap->Center0[j].m_nY == Center_y0 - Card_Up)
			{			
				pCardsMap->Center0[j].m_nY += Card_Up;	
			}
		}
		pCardsMap->m_nUpCounter = 0;  //提起牌数为0;
		::RedrawGame();
		m_bButton = 0;   //延时锁;
		Sleep(400);
		m_bButton = 1;   //延时锁;
		return 0;
	}
	return -1;
}
//右边玩家（电脑）思考，并决定要出的牌;		
int CClientGame::RightDoing()
{
	
	if( m_nLordLock == 2 )
	{		
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;
	}

	if( pLeftCards->m_cDiscardingType.m_nTypeNum == 0 && 
		pCenterCards->m_cDiscardingType.m_nTypeNum == 0 )
	{
		m_nLordLock = -1;
	}

	//补丁,调整出牌显示;
	int Temp_Discarding_Counter = 0;  //刚出的牌的数量;
	CCard Temp_Discarding[20];    //刚出的牌;

	pClientLink->Recv(Temp_Discarding,sizeof(CCard)*20);
	pClientLink->Recv(&Temp_Discarding_Counter,sizeof(int));
	pClientLink->Recv(&pRightCards->m_cDiscardingType,sizeof(CCardsType));	

	if( m_nRoundCounter == 3 )  //说明已经出了牌;
	{
		//把已经出的牌回退;		
		if(pRightCards->m_nDiscardingCounter != 0)
			pDrawCards->RightBacking();

		if(pCenterCards->m_nDiscardingCounter != 0)
			pDrawCards->CenterBacking();

		if(pLeftCards->m_nDiscardingCounter != 0)
			pDrawCards->LeftBacking();

		m_nRoundCounter = 0;   //新的一圈;
	}
	m_nRoundCounter++;

	//更新数据;
	for(int i=0;i<20;i++)
	{
		pRightCards->m_cDiscarding[i] = Temp_Discarding[i];
	}
	pRightCards->m_nDiscardingCounter = Temp_Discarding_Counter;

	if( pRightCards->m_cDiscardingType.m_nTypeNum == 4 )
	{
		m_nBombCounter++;//加分;
	}
	if( pRightCards->m_cDiscardingType.m_nTypeNum != 0 )
	{
		pDrawCards->RightDiscarding();
		::RedrawGame();
		if( pRightCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			AccountScore(2);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 0;
		}
	}
	else
	{
		if( m_nCurrentLord == 2 )
		{
			m_nLordLock = 2;
		}
		//打印右边玩家不要信息;
		pRightCards->m_cDiscardingType.m_nTypeNum = 0;
		pDraw->TextGDI("过牌!－",600,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
		Sleep(300);
	}
	return 1;
}		
//客户端决定地主;
int CClientGame::DecideLord()	   
{
	pClientLink->Recv(&m_nDefaultLord,sizeof(int));

	m_nCurrentLord = m_nDefaultLord;  //当前地主;
	pDrawItem->BringRandLord(m_nDefaultLord);

	//info_type = 0: "%s要当地主,本局地主是:%s"
	//info_type = 1: "%s不当地主,等待%s选择..."
	//info_type = 2: "按规则,本局地主是:%s"

	int i;
	int counter = 0;
	while(1)
	{
		if( m_nCurrentLord != 1 )
		{
			pClientLink->Recv(&i,sizeof(int));
			if( i == m_nCurrentLord )
			{
				pDrawItem->GameInfo(m_nCurrentLord,0);
				//跳出,准备开始游戏;
				break;
			}
			else if( i == -1 )
			{
				pDrawItem->GameInfo(m_nCurrentLord,1);
				pClientLink->Recv(&m_nCurrentLord,sizeof(int));
			}
		}
		else if( m_nCurrentLord == 1 )
		{
			pDrawItem->ChooseLord();
			while(1)
			{
				if( pInput->IsLButtonDown(m_cYES_RECT) )  //要
				{
					PlaySound(MAKEINTRESOURCE(IDR_CHOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  
					pDrawItem->GameInfo(m_nCurrentLord,0);								
					pClientLink->Send(&m_nCurrentLord,sizeof(int));
					//跳出,准备开始游戏;goto...
					goto out;
				}
				else if( pInput->IsLButtonDown(m_cNO_RECT) )  //不要
				{
					PlaySound(MAKEINTRESOURCE(IDR_CHOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
					pDrawItem->GameInfo(m_nCurrentLord,1);
					i = -1;
					pClientLink->Send(&i,sizeof(int));

					pClientLink->Recv(&m_nCurrentLord,sizeof(int));
					break;
				}
				Sleep(50);   //产生一个延时;
			}//end while;
		}
		counter++;
		if( counter == 3 )
		{
			pDrawItem->GameInfo(m_nDefaultLord,2);
			m_nCurrentLord = m_nDefaultLord;
			//跳出,准备开始游戏;
			break;
		}
	}//end while;
out:
//	Sleep(10000);
	return 1;
}

int CClientGame::Run()
{
	//开始新的牌局;
	NewGame();		   
	//显示局数;
	pDrawItem->GameCounter(m_nGameCounter);

	//接受服务器发牌;
	pClientLink->Recv(g_cAllCards,sizeof(CCard)*54);
	//发牌;
	pDrawCards->Dealing();   

	DecideLord();

	pDrawCards->DealingLord(m_nCurrentLord); //发地主牌;

	m_bOperate  = TRUE;
//开始循环;
	if(m_nCurrentLord == 0)
	{
		LeftDoing();
		::SetTimer(hWnd,20,10,NULL);
	}
	else if(m_nCurrentLord == 1)
	{
		::SetTimer(hWnd,20,10,NULL);
	}
	else if(m_nCurrentLord == 2)
	{
		RightDoing();
		LeftDoing();
		::SetTimer(hWnd,20,10,NULL);
	}
	return 1;
}