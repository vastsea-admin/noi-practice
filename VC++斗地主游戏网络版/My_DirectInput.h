//Download by http://www.NewXing.com
// My_DirectInput.h: interface for the CMy_DirectInput class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MY_DIRECTINPUT_H__DB0A52DA_F23D_4F2F_8A6E_24A806C9AD1D__INCLUDED_)
#define AFX_MY_DIRECTINPUT_H__DB0A52DA_F23D_4F2F_8A6E_24A806C9AD1D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
class CMainFrame;

 
class CMyDirectInput  
{
private:
	LPDIRECTINPUT7	pDI; 
//键盘设备接口;
	LPDIRECTINPUTDEVICE7	pDI_Keyborad; 
//鼠标设备接口;	
	LPDIRECTINPUTDEVICE7	pDI_Mouse;

	DIMOUSESTATE2	MState; 
	HRESULT	result; 
public:

	int KeyboradInit();
	int MouseInit();

	CMyDirectInput();
	virtual ~CMyDirectInput();

	int IsLButtonDown(const CRect&);
	int IsRButtonDown(const CRect&);
};

#endif // !defined(AFX_MY_DIRECTINPUT_H__DB0A52DA_F23D_4F2F_8A6E_24A806C9AD1D__INCLUDED_)
