//Download by http://www.NewXing.com

typedef unsigned short USHORT;   //类型定义
typedef unsigned short WORD;
typedef unsigned char  UCHAR;
typedef unsigned char  BYTE;

#define BITMAP_ID            0x4D42 // universal id for a bitmap

//#define MAX_FRAMES      6

#define DDRAW_INIT_STRUCT(ddstruct) { memset(&ddstruct,0,sizeof(ddstruct)); ddstruct.dwSize=sizeof(ddstruct); }

// this builds a 16 bit color value in 5.5.5 format (1-bit alpha mode)
#define _RGB16BIT555(r,g,b) ((b & 31) + ((g & 31) << 5) + ((r & 31) << 10))

// this builds a 16 bit color value in 5.6.5 format (green dominate mode)
#define _RGB16BIT565(r,g,b) ((b & 31) + ((g & 63) << 5) + ((r & 31) << 11))

// this builds a 32 bit color value in A.8.8.8 format (8-bit alpha mode)
#define _RGB32BIT(a,r,g,b) ((b) + ((g) << 8) + ((r) << 16) + ((a) << 24))

// container structure for bitmaps .BMP file
typedef struct BITMAP_FILE_TAG
{
	BITMAPFILEHEADER bitmapfileheader;  // this contains the bitmapfile header
	BITMAPINFOHEADER bitmapinfoheader;  // this is all the info including the palette
	PALETTEENTRY     palette[256];      // we will store the palette here
	UCHAR            *buffer;           // this is a pointer to the data

}BITMAP_FILE, *BITMAP_FILE_PTR;

class CMainFrame;

class CMyDirectDraw
{
//	friend class CGame;
private:
	LPDIRECTDRAW7         lpdd;					 // dd object

	LPDIRECTDRAWCLIPPER   lpddclipper;			 // dd clipper

	DDSURFACEDESC2        ddsd;                  // a direct draw surface description struct
	DDSCAPS2              ddscaps;               // a direct draw surface capabilities struct

	DDBLTFX               ddbltfx;               // used to fill
	HRESULT               ddrval;                // result back from dd calls
	DWORD                 start_clock_count;	 // used for timing

	BITMAP_FILE           bitmap;                // holds the bitmap

	CFont* old_font;

	int SCREEN_BPP;  //32位色;

public:
	int Flip_Bitmap(UCHAR *image,int bytes_per_line, int height);

	int Load_Bitmap_File(BITMAP_FILE_PTR bitmap, char *filename);

	int Unload_Bitmap_File(BITMAP_FILE_PTR bitmap);

	int Scan_Image_Bitmap(BITMAP_FILE_PTR bitmap,
						  LPDIRECTDRAWSURFACE7 lpdds,
						  int cx,
						  int cy
						  );
	LPDIRECTDRAWCLIPPER AttachClipper(LPDIRECTDRAWSURFACE7 lpdds,
											 int num_rects,
											 LPRECT clip_list);
public:
	CMyDirectDraw();
	virtual ~CMyDirectDraw();

	int BitmapToSurface(LPDIRECTDRAWSURFACE7 Dest_Surface,  //目的表面;
							 int Dest_x,		//目的表面x座标;
							 int Dest_y,		//目的表面y座标;
							 char* filename,	//源位图文件名;	
							 int Bitmap_Width,	//位图宽;
							 int Bitmap_Height	//位图长;
							 );

	int Init();   //初始化DDraw 平台;


//单色填充整个表面
	int FillSurface(LPDIRECTDRAWSURFACE7 lpdds,int color);

	LPDIRECTDRAWSURFACE7 CreateSurface(int width,
									   int height,
									   int mem_flags,
									   int color_key
											  );

	int DrawSurface(LPDIRECTDRAWSURFACE7 source,
						   int x,
						   int y, 
						   int width,
						   int height,
						   LPDIRECTDRAWSURFACE7 dest, 
						   int transparent
						   );    

	int TextGDI(char *text,
					  int x,
					  int y,
					  COLORREF color,
					  int size,
					  char* type,
					  LPDIRECTDRAWSURFACE7 lpdds);

	int DebugText(char* text,int time);  //调试用;

};

