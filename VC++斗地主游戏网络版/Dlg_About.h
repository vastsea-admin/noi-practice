//Download by http://www.NewXing.com
#if !defined(AFX_DLG_ABOUT_H__6C8AE14B_FE38_4F8F_811C_7C08821F77E0__INCLUDED_)
#define AFX_DLG_ABOUT_H__6C8AE14B_FE38_4F8F_811C_7C08821F77E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dlg_About.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlg_About dialog

class CDlg_About : public CDialog
{
// Construction
public:
	CDlg_About(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlg_About)
	enum { IDD = IDD_ABOUT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlg_About)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlg_About)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLG_ABOUT_H__6C8AE14B_FE38_4F8F_811C_7C08821F77E0__INCLUDED_)
