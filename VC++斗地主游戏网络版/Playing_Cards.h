//Download by http://www.NewXing.com
// Playing_Cards.h: interface for the CPlaying_Cards class.
//
//////////////////////////////////////////////////////////////////////
//

//牌型数据结构;
class CCardsType
{
public:
	int m_nTypeNum;
	int m_nTypeValue;
public:
	CCardsType();
};

/*						牌型及牌值定义

		     牌型(Cards_Type)		  牌值(Cards_Value)           牌数:
单牌:		 1							CCard.Value(面值)           1
一对:		 2							CCard.Value(面值)			2
三张:		 3							CCard.Value(面值)			3

三带一:		 31							三张的Card.Value(面值)		4
四张:		 4							CCard.Value(面值)			4

单顺:		 5							最小牌的Card.Value(面值)    5
三带一对:	 32							三张的Card.Value(面值)		5		

单顺:		 6							最小牌的Card.Value(面值)    6
双顺:		 222						最小牌的Card.Value(面值)    6
三顺:	     33							最小牌的Card.Value(面值)    6
四带二单:	 411						四张的Card.Value(面值)		6	 

单顺:		 7							最小牌的Card.Value(面值)    7

单顺:		 8							最小牌的Card.Value(面值)    8
双顺		 2222						最小牌的Card.Value(面值)    8
三顺带二单:  3311						最小三张的Card.Value(面值)  8
四带二对:	 422						四张的Card.Value(面值)		8

单顺:		 9							最小牌的Card.Value(面值)    9
三顺:		 333						最小三张的Card.Value(面值)  9

单顺:		 10							最小牌的Card.Value(面值)    10
双顺:		 22222						最小牌的Card.Value(面值)    10
三顺带二对:  3322						最小三张的Card.Value(面值)  10

单顺:		 11							最小牌的Card.Value(面值)    11

单顺:		 12							最小牌的Card.Value(面值)    12
双顺:		 222222						最小对牌的Card.Value(面值)  12
三顺:		 3333						最小三张的Card.Value(面值)  12
三顺带三:	 333111						最小三张的Card.Value(面值)  12

双顺		 2222222					最小对牌的Card.Value(面值)  14

三顺带三对:  333222						最小三张的Card.Value(面值)  15
三顺:		 33333						最小三张的Card.Value(面值)  15

双顺		 22222222					最小对牌的Card.Value(面值)  16
三顺带四单:  33331111					最小三张的Card.Value(面值)  16

双顺		 222222222					最小对牌的Card.Value(面值)  18
三顺		 333333						最小三张的Card.Value(面值)  18

双顺		 2222222222					最小对牌的Card.Value(面值)  20
三顺带五单:	 3333311111					最小三张的Card.Value(面值)  20
三顺带四对:  33332222					最小三张的Card.Value(面值)  20

*/
class CCard
{
public:
	int m_nColor;  //花色;
	int m_nValue;  //面值;
public:
	CCard();
};

class CPlayingCards  
{
public:

	int m_nCardsTable[17];	//记录未出牌的表;

	int	m_nCardsCounter;		 //未出的牌的数量;
	CCard m_cCards[20];		 //未出的牌;

	int m_nDiscardedCounter;	 //已经出的牌数量;
	CCard m_cDiscarded[20];     //已经出的牌;

	CCardsType m_cDiscardingType;   //刚出牌的牌型和大小;
	int m_nDiscardingCardsTable[17];     //记录出牌类型的扫描表;Scan_Cards_Table[3]记录3的数量,以此类推;

	int m_nDiscardingCounter;  //刚出的牌的数量;
	CCard m_cDiscarding[20];    //刚出的牌;

	int m_nChoosingCardsCounter;   //选择要出的牌型;
	CCard m_cChoosingCards[20];	  //选择要出的牌数组;
	
	const CCard m_cBLANK_CARD;      //表示空白牌的常量;
private:

	int Is2();			  //判断是否是一对牌; 
	int Is3();			  //判断是否是三张;
	int IsBomb();		  //判断是否是四张(炸弹); 

	int IsSeries();		  //判断是否是单顺(拖拉机),此函数适合5-12张牌情况; 
	int IsDoubleSeries(); //判断是否是双顺;此函数适合6-20张牌情况;
	int IsThreeSeries();  //判断是否是三顺;此函数适合6-18张牌情况;

	int Is31();			  //判断是否是三带一单;
	int Is32();		      //判断是否是三带一对;
	int Is411();		  //判断是否是四带两单;
	int Is3311();		  //判断是否是三顺带两单;
	int Is422();		  //判断是否是四带两对;
	int Is3322();		  //判断是否是三顺带两对;
	int Is333111();		  //判断是否是三顺带三单;
	int Is333222();		  //判断是否是三顺带三对;
	int Is33331111();	  //判断是否是三顺带四单;

	int Search1(CCardsType& cCardsType);   //寻找单牌出;
	int Search2(CCardsType& cCardsType);   //寻找对牌出;
	int Search3(CCardsType& cCardsType);   //寻找三张出;

	int SearchBomb(CCardsType& cCardsType);   //寻找炸弹出;
	int Search31(CCardsType& cCardsType);     //寻找三带1出;
	int Search32(CCardsType& cCardsType);     //寻找三带2出;
	int Search411(CCardsType& cCardsType);    //寻找四带2单;
	int Search422(CCardsType& cCardsType);	//寻找四带两对;
	int Search3311(CCardsType& cCardsType);	//寻找三顺带两单;
	int Search3322(CCardsType& cCardsType);	//寻找三顺带两对;

	int Search222(CCardsType& cCardsType);	//寻找双顺;
	int Search2222(CCardsType& cCardsType);	//寻找双顺;
//拖拉机(单顺)
	int Search5(CCardsType& cCardsType);    //寻找5单顺出;
	int Search6(CCardsType& cCardsType);    //寻找6单顺出;
	int Search7(CCardsType& cCardsType);    //寻找7单顺出;
	int Search8(CCardsType& cCardsType);    //寻找8单顺出;
	int Search9(CCardsType& cCardsType);    //寻找9单顺出;
	 //寻找大于等于10单顺出;
	int SearchBeyond10(CCardsType& cCardsType,int nLong);

	BOOL AI_Patch1();                       //工人智能补丁1;
public:
	CPlayingCards();
	virtual ~CPlayingCards();

	void New();	
	void ScanToTable();

	int AccountCardsValue();  //计算牌值; 

	int CleanUp();    //整理牌数据结构;

	int CheckChoosing();	//检查出牌的逻辑合法性;

	int DirectChoose();		//直接选择出牌;
	int CompareChoose(CCardsType&);		//比较大小后决定不要或者出牌; 
};
