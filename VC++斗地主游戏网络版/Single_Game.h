//Download by http://www.NewXing.com
// Single_Game.h: interface for the CSingle_Game class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SINGLE_GAME_H__B7A7BB67_FC44_4FCB_8DC8_4CD117D05C5B__INCLUDED_)
#define AFX_SINGLE_GAME_H__B7A7BB67_FC44_4FCB_8DC8_4CD117D05C5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSingleGame : public CGame 
{
private:
	int DecideLord();			//决定地主;

	virtual int LeftDoing();	    //左边（电脑）思考，并决定要出的牌;
	virtual int CenterDoing();		//中间(本机)玩家出牌;
	virtual int RightDoing();	    //右边（电脑）思考，并决定要出的牌;
	//奇怪!居然可以将虚拟函数声明成私有?
	virtual int Run();			//整个游戏过程的中枢;
public:
	CSingleGame();
	virtual ~CSingleGame();
};

#endif // !defined(AFX_SINGLE_GAME_H__B7A7BB67_FC44_4FCB_8DC8_4CD117D05C5B__INCLUDED_)
