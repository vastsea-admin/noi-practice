//Download by http://www.NewXing.com
// Server_Game.h: interface for the CServer_Game class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVER_GAME_H__9ED95B0F_62F7_49EF_8185_819A9E4894CC__INCLUDED_)
#define AFX_SERVER_GAME_H__9ED95B0F_62F7_49EF_8185_819A9E4894CC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CServerGame : public CGame 
{
private:
	int SentCards();	//服务器独有的函数;
	int DecideLord();			//决定地主;

	virtual int LeftDoing();		//左边玩家（电脑）思考，并决定要出的牌;
	virtual int CenterDoing();		//本机玩家出牌;
	virtual int RightDoing();		//右边玩家（电脑）思考，并决定要出的牌;
	//奇怪!居然可以将虚拟函数声明成私有?
	virtual int Run();			//整个游戏过程的中枢;
public:
	CServerGame();
	virtual ~CServerGame();
};

#endif // !defined(AFX_SERVER_GAME_H__9ED95B0F_62F7_49EF_8185_819A9E4894CC__INCLUDED_)
