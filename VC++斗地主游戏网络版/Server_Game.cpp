//Download by http://www.NewXing.com
// Server_Game.cpp: implementation of the CServerGame class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "斗地主.h"

#include "Game.h"

#include "Link.h"
#include "My_DirectInput.h"
#include "my_directdraw.h"

#include "Playing_Cards.h"

#include "Draw_Item_Engine.h"
#include "Draw_Cards_Engine.h"

#include "Server_Game.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CServerGame::CServerGame()
{

}

CServerGame::~CServerGame()
{

}
//左边玩家思考，并决定要出的牌;
int CServerGame::LeftDoing()
{	
	if( m_nLordLock == 0 )
	{		
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;
	}

	if( pRightCards->m_cDiscardingType.m_nTypeNum == 0 && 
		pCenterCards->m_cDiscardingType.m_nTypeNum == 0 )
	{
		m_nLordLock = -1;
	}

	//补丁,调整出牌显示;
	int Temp_Discarding_Counter = 0;  //刚出的牌的数量;
	CCard Temp_Discarding[20];//刚出的牌;

	pServerLink2->Recv(Temp_Discarding,sizeof(CCard)*20);
	pServerLink2->Recv(&Temp_Discarding_Counter,sizeof(int));
	pServerLink2->Recv(&pLeftCards->m_cDiscardingType,sizeof(CCardsType));	

	if( m_nRoundCounter == 3 )  //说明已经出了牌;
	{
		//把已经出的牌回退;
		if(pLeftCards->m_nDiscardingCounter != 0)
			pDrawCards->LeftBacking();

		if(pCenterCards->m_nDiscardingCounter != 0)
			pDrawCards->CenterBacking();

		if(pRightCards->m_nDiscardingCounter != 0)
			pDrawCards->RightBacking();

		m_nRoundCounter = 0;   //新的一圈;
	}
	m_nRoundCounter++;

	//更新数据;
	for(int i=0;i<20;i++)
	{
		pLeftCards->m_cDiscarding[i] = Temp_Discarding[i];
	}
	pLeftCards->m_nDiscardingCounter = Temp_Discarding_Counter;

	pServerLink1->Send(pLeftCards->m_cDiscarding,sizeof(CCard)*20);
	pServerLink1->Send(&pLeftCards->m_nDiscardingCounter,sizeof(int));
	pServerLink1->Send(&pLeftCards->m_cDiscardingType,sizeof(CCardsType));	

	if( pLeftCards->m_cDiscardingType.m_nTypeNum == 4 )
	{
		m_nBombCounter++;//加分;
	}

	if(pLeftCards->m_cDiscardingType.m_nTypeNum != 0)
	{
		pDrawCards->LeftDiscarding();
		::RedrawGame();
		if( pLeftCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			AccountScore(0);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 0;
		}
	}
	else
	{
		//显示不要;
		pDraw->TextGDI("过牌!－",250,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
		if( m_nCurrentLord == 0 )
		{
			m_nLordLock = 0;
		}
		Sleep(300);
	}
	return 1;
}
//中间(本机)玩家出牌;
int CServerGame::CenterDoing()
{	
	if( m_nLordLock == 1 )
	{
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;
			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;  //表示当前是地主,上次轮过牌,现在则不能出牌;
	}

	//如果没有选择牌;
	if( pCardsMap->m_nUpCounter == 0 )
	{
		//当不是第一个出牌的时候才可以过牌;
		if( pLeftCards->m_cDiscardingType.m_nTypeNum != 0 || 
			pRightCards->m_cDiscardingType.m_nTypeNum != 0 )
		{
			if( m_nRoundCounter == 3 )  //说明已经出了牌;
			{
				//把已经出的牌回退;
				if(pCenterCards->m_nDiscardingCounter != 0)
					pDrawCards->CenterBacking();

				if(pRightCards->m_nDiscardingCounter != 0)
					pDrawCards->RightBacking();

				if(pLeftCards->m_nDiscardingCounter != 0)
					pDrawCards->LeftBacking();

				m_nRoundCounter = 0;   //新的一圈;
			}
			m_nRoundCounter++;

			pCenterCards->m_cDiscardingType.m_nTypeNum = 0; //将出的牌值设为空;

			pServerLink1->Send(pCenterCards->m_cDiscarding,sizeof(CCard)*20);
			pServerLink1->Send(&pCenterCards->m_nDiscardingCounter,sizeof(int));
			pServerLink1->Send(&pCenterCards->m_cDiscardingType,sizeof(CCardsType));	

			pServerLink2->Send(pCenterCards->m_cDiscarding,sizeof(CCard)*20);
			pServerLink2->Send(&pCenterCards->m_nDiscardingCounter,sizeof(int));
			pServerLink2->Send(&pCenterCards->m_cDiscardingType,sizeof(CCardsType));

			if( m_nCurrentLord == 1 )
			{
				m_nLordLock = 1;
			}

			pDraw->TextGDI("过牌!",470,400,RGB(255,0,0),400,"华文新魏",lpddsprimary);
			Sleep(500);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	pCenterCards->m_nChoosingCardsCounter = 0;
	//将要出的牌加入到m_cDiscarding[20]数组中,m_nDiscardingCounter计数增加;
	for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
	{
		if(pCardsMap->Center0[i].m_nY== Center_y0 - Card_Up)
		{			
			pCenterCards->m_cChoosingCards[pCenterCards->m_nChoosingCardsCounter] = pCenterCards->m_cCards[i];
			pCenterCards->m_nChoosingCardsCounter++;
		}
	}

	//判断出牌的合法性,
	if( pCenterCards->CheckChoosing() )
	{
		//大小判断;
		if( CompareCards() == 0 )
		{
			for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
			{
				if(pCardsMap->Center0[j].m_nY== Center_y0 - Card_Up)
				{			
					pCardsMap->Center0[j].m_nY+= Card_Up;	
				}
			}		
			pCardsMap->m_nUpCounter = 0;  //提起牌数为0;
			::RedrawGame();
			m_bButton = 0;   //延时锁;
			Sleep(400);
			m_bButton = 1;   //延时锁;
			return 0;
		}

		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;
			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}	
		m_nRoundCounter++;

		//将要出的牌加入到m_cDiscarding[20]数组中,m_nDiscardingCounter计数增加;
		for(int i=0;i<pCenterCards->m_nCardsCounter;i++)
		{
			if(pCardsMap->Center0[i].m_nY== Center_y0 - Card_Up)
			{			
				pCenterCards->m_cDiscarding[pCenterCards->m_nDiscardingCounter] = pCenterCards->m_cCards[i];
				pCenterCards->m_nDiscardingCounter++;
			}
		}

		if( pCenterCards->m_cDiscardingType.m_nTypeNum == 4 )
		{
			m_nBombCounter++;//加分;
		}
		//发送给其他两个玩家;
		pServerLink1->Send(pCenterCards->m_cDiscarding,sizeof(CCard)*20);
		pServerLink1->Send(&pCenterCards->m_nDiscardingCounter,sizeof(int));
		pServerLink1->Send(&pCenterCards->m_cDiscardingType,sizeof(CCardsType));	

		pServerLink2->Send(pCenterCards->m_cDiscarding,sizeof(CCard)*20);
		pServerLink2->Send(&pCenterCards->m_nDiscardingCounter,sizeof(int));
		pServerLink2->Send(&pCenterCards->m_cDiscardingType,sizeof(CCardsType));	

		pDrawCards->CenterDiscarding();  //本机玩家出牌;
		::RedrawGame();
		if( pCenterCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			PlaySound(MAKEINTRESOURCE(IDR_WIN),AfxGetResourceHandle(),
					SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
			AccountScore(1);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 2;
		}
	}
	else
	{
		for(int j=0;j<pCenterCards->m_nCardsCounter;j++)
		{
			if(pCardsMap->Center0[j].m_nY== Center_y0 - Card_Up)
			{			
				pCardsMap->Center0[j].m_nY+= Card_Up;	
			}
		}
		pCardsMap->m_nUpCounter = 0;  //提起牌数为0;
		::RedrawGame();
		m_bButton = 0;   //延时锁;
		Sleep(400);
		m_bButton = 1;   //延时锁;
		return 0;
	}
	return 1;
}
//右边玩家思考，并决定要出的牌;		
int CServerGame::RightDoing()
{
	
	if( m_nLordLock == 2 )
	{		
		if( m_nRoundCounter == 3 )  //说明已经出了牌;
		{
			//把已经出的牌回退;

			if(pLeftCards->m_nDiscardingCounter != 0)
				pDrawCards->LeftBacking();

			if(pCenterCards->m_nDiscardingCounter != 0)
				pDrawCards->CenterBacking();

			if(pRightCards->m_nDiscardingCounter != 0)
				pDrawCards->RightBacking();

			m_nRoundCounter = 0;   //新的一圈;
		}
		m_nRoundCounter++;
		return -1;
	}

	if( pLeftCards->m_cDiscardingType.m_nTypeNum == 0 && 
		pCenterCards->m_cDiscardingType.m_nTypeNum == 0 )
	{
		m_nLordLock = -1;
	}

	//补丁,调整出牌显示;
	int Temp_Discarding_Counter = 0;  //刚出的牌的数量;
	CCard Temp_Discarding[20];		  //刚出的牌;

	pServerLink1->Recv(Temp_Discarding,sizeof(CCard)*20);
	pServerLink1->Recv(&Temp_Discarding_Counter,sizeof(int));
	pServerLink1->Recv(&pRightCards->m_cDiscardingType,sizeof(CCardsType));	

	if( m_nRoundCounter == 3 )  //说明已经出了牌;
	{
		//把已经出的牌回退;		
		if(pRightCards->m_nDiscardingCounter != 0)
			pDrawCards->RightBacking();

		if(pCenterCards->m_nDiscardingCounter != 0)
			pDrawCards->CenterBacking();

		if(pLeftCards->m_nDiscardingCounter != 0)
			pDrawCards->LeftBacking();

		m_nRoundCounter = 0;   //新的一圈;
	}
	m_nRoundCounter++;

	//更新数据;
	for(int i=0;i<20;i++)
	{
		pRightCards->m_cDiscarding[i] = Temp_Discarding[i];
	}
	pRightCards->m_nDiscardingCounter = Temp_Discarding_Counter;	

	pServerLink2->Send(pRightCards->m_cDiscarding,sizeof(CCard)*20);
	pServerLink2->Send(&pRightCards->m_nDiscardingCounter,sizeof(int));
	pServerLink2->Send(&pRightCards->m_cDiscardingType,sizeof(CCardsType));
	
	if( pRightCards->m_cDiscardingType.m_nTypeNum == 4 )
	{
		m_nBombCounter++;//加分;	
	}

	if(pRightCards->m_cDiscardingType.m_nTypeNum != 0)
	{
		pDrawCards->RightDiscarding();
		::RedrawGame();
		if( pRightCards->m_nCardsCounter == 0 )
		{
			//打印结束信息;
			AccountScore(2);
			::SetTimer(hWnd,0,0,NULL);   //开始游戏;
			return 0;
		}
	}
	else
	{
		if( m_nCurrentLord == 2 )
		{
			m_nLordLock = 2;
		}

		pRightCards->m_cDiscardingType.m_nTypeNum = 0;
		pDraw->TextGDI("过牌!－",600,200,RGB(255,0,0),400,"华文新魏",lpddsprimary);
		Sleep(300);
	}
	return 1;
}
//传送牌数据结构给两个客户;
int CServerGame::SentCards()
{
	CCard All_Send_Cards[54];

	All_Send_Cards[51] = g_cAllCards[51];
	All_Send_Cards[52] = g_cAllCards[52];
	All_Send_Cards[53] = g_cAllCards[53];

	int i;
	for(i=0;i<17;i++)
	{
		All_Send_Cards[i*3]   = g_cAllCards[i*3+1];
		All_Send_Cards[i*3+1] = g_cAllCards[i*3+2];
		All_Send_Cards[i*3+2] = g_cAllCards[i*3];
	}

	pServerLink1->Send(All_Send_Cards,sizeof(CCard)*54);

	for(i=0;i<17;i++)
	{
		All_Send_Cards[i*3]   = g_cAllCards[i*3+2];
		All_Send_Cards[i*3+1] = g_cAllCards[i*3];
		All_Send_Cards[i*3+2] = g_cAllCards[i*3+1];
	}

	pServerLink2->Send(All_Send_Cards,sizeof(CCard)*54);
	return 1;
}
//服务器决定地主;
int CServerGame::DecideLord()	   
{
	m_nDefaultLord = rand()%3;
	m_nCurrentLord = m_nDefaultLord;

	int i;
	switch(	m_nCurrentLord )
	{
	case 0:
		i = 2;
		pServerLink1->Send(&i,sizeof(int));
		i = 1;
		pServerLink2->Send(&i,sizeof(int));
		break;
	case 1:
		i = 0;
		pServerLink1->Send(&i,sizeof(int));
		i = 2;
		pServerLink2->Send(&i,sizeof(int));
		break;
	case 2:
		i = 1;
		pServerLink1->Send(&i,sizeof(int));
		i = 0;
		pServerLink2->Send(&i,sizeof(int));
		break;
	}

	pDrawItem->BringRandLord(m_nDefaultLord);

	int counter = 0;

	while(1)
	{
		if( m_nCurrentLord == 1 )
		{
			pDrawItem->ChooseLord();
			while(1)
			{
				if( pInput->IsLButtonDown(m_cYES_RECT) )  //要
				{
					PlaySound(MAKEINTRESOURCE(IDR_CHOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
					pDrawItem->GameInfo(m_nCurrentLord,0);								
					i = 0;
					pServerLink1->Send(&i,sizeof(int));
					i = 2;
					pServerLink2->Send(&i,sizeof(int));
					//跳出,准备开始游戏;goto...
					goto out;
				}
				else if( pInput->IsLButtonDown(m_cNO_RECT) )  //不要
				{
					PlaySound(MAKEINTRESOURCE(IDR_CHOSE),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
					pDrawItem->GameInfo(m_nCurrentLord,1);
					i = -1;
					pServerLink1->Send(&i,sizeof(int));
					pServerLink2->Send(&i,sizeof(int));

					i = 1;
					pServerLink1->Send(&i,sizeof(int));
					i = 0;
					pServerLink2->Send(&i,sizeof(int));

					m_nCurrentLord = 2;   //自己更新
					break;
				}
				Sleep(50);   //产生一个延时;
			}//end while;
		}
		else if( m_nCurrentLord != 1 )
		{
			if( m_nCurrentLord == 2 )
			{
				pServerLink1->Recv(&i,sizeof(int));			
				if( i == 1 )
				{
					pDrawItem->GameInfo(m_nCurrentLord,0);
					i = 0;
					pServerLink2->Send(&i,sizeof(int));
					//跳出,准备开始游戏;
					break;
				}
				else if( i == -1 )
				{
					pDrawItem->GameInfo(m_nCurrentLord,1);
					i = -1;
					pServerLink2->Send(&i,sizeof(int));
					
					m_nCurrentLord = 0;  //自己更新;
					//更新当前地主;
					i = 2;
					pServerLink1->Send(&i,sizeof(int));
					i = 1;
					pServerLink2->Send(&i,sizeof(int));
				}
			}
			else if( m_nCurrentLord == 0 )
			{
				pServerLink2->Recv(&i,sizeof(int));
				if( i == 1 )
				{
					pDrawItem->GameInfo(m_nCurrentLord,0);
					i = 2;
					pServerLink1->Send(&i,sizeof(int));
					//跳出,准备开始游戏;
					break;
				}
				else if( i == -1 )
				{
					pDrawItem->GameInfo(m_nCurrentLord,1);
					i = -1;
					pServerLink1->Send(&i,sizeof(int));
					
					m_nCurrentLord = 1;  //自己更新;
					//更新当前地主;
					i = 0;
					pServerLink1->Send(&i,sizeof(int));
					i = 2;
					pServerLink2->Send(&i,sizeof(int));
				}
			}				
		}
		counter++;
		if( counter == 3 )
		{
			pDrawItem->GameInfo(m_nDefaultLord,2);
			m_nCurrentLord = m_nDefaultLord;
			//跳出,准备开始游戏;
			break;
		}
	}
out:
//	Sleep(10000);
	return 1;
}
//开始游戏
int CServerGame::Run()
{
	//开始新的牌局;
	NewGame();		   
	//显示局数;
	pDrawItem->GameCounter(m_nGameCounter);  
	//洗牌;
	ShuffleCards();

	//传送牌数据结构给两个客户;
	SentCards();

	pDrawCards->Dealing();   //发牌;

	DecideLord();	   //决定地主;

	pDrawCards->DealingLord(m_nCurrentLord); //发地主牌;

	m_bOperate  = TRUE;
//开始循环;
	if(m_nCurrentLord == 0)
	{
		LeftDoing();
		::SetTimer(hWnd,20,10,NULL);
	}
	else if(m_nCurrentLord == 1)
	{
		::SetTimer(hWnd,20,10,NULL);
	}
	else if(m_nCurrentLord == 2)
	{
		RightDoing();
		LeftDoing();
		::SetTimer(hWnd,20,10,NULL);
	}
	
	return 1;
}