//Download by http://www.NewXing.com
// My_DirectInput.cpp: implementation of the CMyDirectInput class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "斗地主.h"
#include "MainFrm.h"

#include "Game.h"
#include "My_directdraw.h"
#include "My_DirectInput.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyDirectInput::CMyDirectInput()
{
	pDI = NULL;       
	pDI_Keyborad = NULL; 
	pDI_Mouse = NULL; 
}

CMyDirectInput::~CMyDirectInput()
{
	if(pDI_Keyborad)
	{
		pDI_Keyborad->Release();
		pDI_Keyborad = NULL;
	}

	if(pDI_Mouse)
	{
		pDI_Mouse->Release();
		pDI_Mouse = NULL;
	}

	if(pDI)
	{
		pDI->Release();
		pDI = NULL;
	}
}

int CMyDirectInput::KeyboradInit()
{
	HINSTANCE hinst = AfxGetInstanceHandle(); //得到程序实例句柄;
	//取得DirectInput组件接口;
	result = DirectInputCreateEx(hinst, DIRECTINPUT_VERSION, 
        IID_IDirectInput7, (void**)&pDI, NULL);

	if(result != DI_OK)
	{
		AfxMessageBox("建立 DirectInput 对象失败!");
	}

	//建立键盘设备;
	result = pDI->CreateDeviceEx(GUID_SysKeyboard,
								 IID_IDirectInputDevice7,
								 (void**)&pDI_Keyborad,
								 NULL); 

	if(result != DI_OK)
	{
		AfxMessageBox("建立键盘输入设备失败!");
	}
	//设置数据格式;
	result = pDI_Keyborad->SetDataFormat(&c_dfDIKeyboard);

	if(result != DI_OK)
	{
		AfxMessageBox("设定数据格式失败!");
	}

	result = pDI_Keyborad->SetCooperativeLevel(pCMainFrame->m_hWnd, 
					DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);

	if(result != DI_OK)
	{
		AfxMessageBox("设定程序协调层级失败!");
	}

	result = pDI_Keyborad->Acquire(); 
	
	if(result != DI_OK)
	{
		AfxMessageBox("取用输入设备失败!");
	}

	
	// TODO: Add your specialized creation code here
	return 1;
}

int CMyDirectInput::MouseInit()
{

	HINSTANCE hinst = AfxGetInstanceHandle();  

	result = DirectInputCreateEx(hinst, DIRECTINPUT_VERSION, 
        IID_IDirectInput7, (void**)&pDI, NULL); 

	if(result != DI_OK)
	{
		AfxMessageBox("建立 DirectInput 对象失败!");
		return 0;
	}

	result = pDI->CreateDeviceEx(GUID_SysMouse,
								 IID_IDirectInputDevice7,
								 (void**)&pDI_Mouse,
								 NULL); 

	if(result != DI_OK)
	{
		AfxMessageBox("建立鼠标输入装置失败!");
		return 0;
	}

	result = pDI_Mouse->SetDataFormat(&c_dfDIMouse2);

	if(result != DI_OK)
	{
		AfxMessageBox("设定数据格式失败!");
		return 0;
	}

	result = pDI_Mouse->SetCooperativeLevel(pCMainFrame->m_hWnd, 
                   DISCL_BACKGROUND | DISCL_NONEXCLUSIVE); 

	if(result != DI_OK)
	{
		AfxMessageBox("设定程序协调层级失败!");
		return 0;
	}

	result = pDI_Mouse->Acquire();
	
	if(result != DI_OK)
	{
		AfxMessageBox("取用输入设备失败!");
		return 0;
	}

	// TODO: Add your specialized creation code here

	return 1;
}
//检查是否在CRect中有鼠标左键按下,如果有,返回1,否则返回0;
int CMyDirectInput::IsLButtonDown(const CRect& rect)
{
	CPoint point;
	DIMOUSESTATE2	MState;
	HRESULT result = pDI_Mouse->GetDeviceState(sizeof(DIMOUSESTATE2),(LPVOID)&MState);

	if(result != DI_OK )
	{
		AfxMessageBox("取得鼠标状态失败!");
		return -1;
	}

	if(MState.rgbButtons[0] & 0x80)  //如果点击键右键的处理情况;
	{
		::GetCursorPos(&point);
		if(point.x >= rect.left &&
		   point.x <= rect.right &&
		   point.y >= rect.top &&
		   point.y <= rect.bottom )
		{
			return 1;
		}
	}
	return 0;
}
//检查是否在CRect中有鼠标右键按下,如果有,返回1,否则返回0;
int CMyDirectInput::IsRButtonDown(const CRect& rect)
{
	CPoint point;
	DIMOUSESTATE2	MState;
	HRESULT result = pDI_Mouse->GetDeviceState(sizeof(DIMOUSESTATE2),(LPVOID)&MState);

	if(result != DI_OK )
	{
		AfxMessageBox("取得鼠标状态失败!");
		return -1;
	}

	if(MState.rgbButtons[1] & 0x80)  //如果点击键右键的处理情况;
	{
		::GetCursorPos(&point);
		if(point.x >= rect.left &&
		   point.x <= rect.right &&
		   point.y >= rect.top &&
		   point.y <= rect.bottom )
		{
			return 1;
		}
	}
	return 0;
}

