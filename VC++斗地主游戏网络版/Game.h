//Download by http://www.NewXing.com

//主窗口句柄;
extern HWND hWnd;
class CMainFrame;
//主窗口类指针;设为全局变量便于各个类访问其方法;
extern CMainFrame* pCMainFrame;

//关于Direct设备的类;

//DirectInput设备接口指针;
class CMyDirectInput;
extern CMyDirectInput* pInput;	
//DirectDraw设备接口指针;	
class CMyDirectDraw;
extern CMyDirectDraw* pDraw;
extern LPDIRECTDRAWSURFACE7  lpddsprimary;		  // 主表面;
extern LPDIRECTDRAWSURFACE7  lpddsback;			  //后缓冲表面;
extern LPDIRECTDRAWSURFACE7  lpddsbg_Game;	      //存放游戏界面的离屏表面;

//游戏主体框架类;
class CGame;
//class CSingle_Game;
extern CGame* pGame;

//一张扑克;
class CCard;
//所有的牌;
extern CCard g_cAllCards[54];	

//存储玩家信息的类;
class CPlayerInfo;
extern CPlayerInfo* pCenterPlayer;
extern CPlayerInfo* pLeftPlayer;
extern CPlayerInfo* pRightPlayer;
	  
//扑克数据和算法类;
class CPlayingCards;
extern CPlayingCards* pLeftCards;     //左边玩家的牌对象;
extern CPlayingCards* pCenterCards;		  //主机玩家的牌对象;
extern CPlayingCards* pRightCards;	  //右边玩家的牌对象;

//关于其他精灵的绘制引擎;
class CDrawItemEngine;
extern CDrawItemEngine* pDrawItem;

//关于扑克绘制的引擎;
class CDrawCardsEngine;
extern CDrawCardsEngine* pDrawCards;
//游戏牌面坐标系统;
class CCardsMap;
extern CCardsMap* pCardsMap;

//网络设备;
class CLink;
extern  CLink* pServerLink1;  //连接客户1;
extern  CLink* pServerLink2;  //连接客户2;
extern  CLink* pClientLink;   //客户端1;

//位置坐标;
extern int  Screen_Width, Screen_Height;  //屏幕长,宽;

extern int  Cards_Width,  Cards_Height;  //扑克长宽;

//中间玩家未出的牌的起始位置;
extern int	Center_x0,		Center_y0;  
//中间玩家已经出过的牌;
extern int  Center_x1,		Center_y1;
//中间玩家刚出的牌;
extern int  Center_x2,	    Center_y2; 

//左边玩家未出的牌;
extern int Left_x0,			Left_y0;	
//左边玩家已经出过的牌;
extern int Left_x1,			Left_y1;	
//左边玩家刚出的牌;
extern int Left_x2,			Left_y2;	

//右边玩家未出的牌;
extern int Right_x0,		Right_y0; 
//右边玩家已经出过的牌;
extern int Right_x1,		Right_y1;  
//右边玩家刚出的牌;
extern int Right_x2,		Right_y2;  

extern int Lord_Card1_x,	 Lord_Card1_y;
extern int Lord_Card2_x,	 Lord_Card2_y;
extern int Lord_Card3_x,	 Lord_Card3_y;

//发牌的起始位置;
extern int	Card_x;		    
extern int  Card_y;
//左,右玩家未出与已出牌的间距:
extern int	Card_Distance0;
//中间玩家未出的牌的间距;
extern int	Card_Distance1;
//刚出的牌的间距
extern int	Card_Distance2;
//中间扑克选择后突出的高度;
extern int	Card_Up;

//游戏屏幕刷新函数;
//bSprimary参数表示是否将重画显示到主表面,默认是显示到主表面;
int RedrawGame(BOOL bSprimary = 1);

class CPlayerInfo
{
public:
	int m_nPlayerType;   //0表示单人游戏,1表示服务器,2表示客户端;
	int m_nFaceID;
	char m_szName[6];
	int m_nScore;		//得分;
	int m_nWin;			//胜;
	int m_nLose;		//负; 
public:
	CPlayerInfo();
};

enum Who_Is_Lord{ no_player = -1, left_player = 0, center_player = 1, right_player = 2 };
//游戏主体框架类;
class CGame
{
public:
	//0代表左边玩家(电脑)是地主;1代表中间(本机)玩家,2代表右边玩家(电脑);
	int m_nCurrentLord;		//当前地主;

protected:
		
	int m_nDefaultLord;		//默认地主;
	int m_nLordLock;		//地主出牌锁定,
							//0表示左边锁定,1表示中间锁定,2表示右边锁定,-1解开;

	BOOL m_bButton;			//延时锁;
	BOOL m_bOperate ;		//是否允许用户选择牌;

	int m_nRoundCounter;    //计算一轮内出牌次数;
	int m_nBombCounter;		//计算炸弹个数;
	int m_nGameCounter;     //计算牌局数; 

    CRect m_cCardsRect;		//未出牌区域;
	CRect m_cCardsUpRect;	//提前牌区域;
	
	//注意:常量大写,加前缀,可加下划线;
	const CRect m_cOK_RECT;		//出牌按钮;  
	const CRect m_cYES_RECT;	//选择是否当地主; 是;
	const CRect m_cNO_RECT;		//选择是否当地主; 否;
	const CRect m_cEXIT_RECT;   //退出游戏;

protected:
	void ShuffleCards();
	void NewGame();			//开始新的牌局,洗牌;			
	int CompareCards();	    //比较当前牌是否为合法;
	void AccountScore(int player);

	virtual int CenterDoing() = 0;		//本机(中间)玩家的出牌处理;
	virtual int LeftDoing() = 0;		//左边玩家（电脑）思考，并决定要出的牌;
	virtual int RightDoing() = 0;		//右边玩家（电脑）思考，并决定要出的牌;

public:
	CGame();
	virtual ~CGame();

	void Init();				//游戏初始化

	int RefreshMyTurn();		//当轮到本机玩家出牌时的屏幕刷新函数;
								//要被其他用户调用,所以设为公有;

	//刷新菜单控制整个游戏生命周期的线程函数;
	static DWORD WINAPI RefreshMenu(LPVOID pParam);

	virtual int Run() = 0;		//整个游戏过程的中枢;
};



