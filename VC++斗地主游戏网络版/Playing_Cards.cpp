//Download by http://www.NewXing.com
// Playing_Cards.cpp: implementation of the CPlayingCards class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "斗地主.h"
#include "MainFrm.h"

#include "Game.h"
#include "My_DirectInput.h"
#include "My_directdraw.h"

#include "Playing_Cards.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCard::CCard()
{
	m_nColor = 4;   //4表示没有任意花色;
	m_nValue = 17;	  //17表示还没有牌;
}

CCardsType::CCardsType()
{
	m_nTypeNum = 0;
	m_nTypeValue = 0;
}

void CPlayingCards::New()
{
	m_nCardsCounter      = 0;
	m_nDiscardedCounter  = 0;	
	m_nDiscardingCounter = 0;  
	m_nChoosingCardsCounter = 0;
	m_cDiscardingType.m_nTypeNum = 0;
	m_cDiscardingType.m_nTypeValue = 0;
}

CPlayingCards::CPlayingCards()
{
	m_nCardsCounter      = 0;
	m_nDiscardedCounter  = 0;	
	m_nDiscardingCounter = 0;  
	m_nChoosingCardsCounter = 0;
}

CPlayingCards::~CPlayingCards()
{

}

int CPlayingCards::CleanUp()
{
	CCard temp_card;
	for(int i=0; i<m_nCardsCounter - 1; i++)  //整理牌的算法
	{
		for(int j=i+1; j<m_nCardsCounter; j++)
		{
			if( (m_cCards[i].m_nValue * 4 + m_cCards[i].m_nColor) > (m_cCards[j].m_nValue * 4 + m_cCards[j].m_nColor) )
			{
				temp_card = m_cCards[i];
				m_cCards[i] =  m_cCards[j];
				m_cCards[j] =  temp_card;
			}
		}//end for;
	}//end for;
	return 1;
}
//判断是否是单顺(拖拉机),此函数适合5-12张牌情况; 
int CPlayingCards::IsSeries()
{
	int m_nTypeValue = 0;	//单牌的起始大小;
	int counter = 0;  //连续单牌的个数;
	int i,j,k;

	for(i=15; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
/*
			char str[400];
			sprintf(str,"m_nDiscardingCardsTable[3] = %d\n\
				m_nDiscardingCardsTable[4] = %d\n\
				m_nDiscardingCardsTable[5] = %d\n\
				m_nDiscardingCardsTable[6] = %d\n\
				m_nDiscardingCardsTable[7] = %d\n\
				m_nDiscardingCardsTable[8] = %d\n\
				m_nDiscardingCardsTable[9] = %d\n\
				m_nDiscardingCardsTable[10] = %d\n\
				m_nDiscardingCardsTable[11] = %d\n\
				m_nDiscardingCardsTable[12] = %d\n\
				m_nDiscardingCardsTable[13] = %d\n\
				m_nDiscardingCardsTable[14] = %d\n",
						m_nDiscardingCardsTable[3],
						m_nDiscardingCardsTable[4],
						m_nDiscardingCardsTable[5],
						m_nDiscardingCardsTable[6],
						m_nDiscardingCardsTable[7],
						m_nDiscardingCardsTable[8],
						m_nDiscardingCardsTable[9],
						m_nDiscardingCardsTable[10],
						m_nDiscardingCardsTable[11],
						m_nDiscardingCardsTable[12],
						m_nDiscardingCardsTable[13],
						m_nDiscardingCardsTable[14]);
//			pMy_Draw->Text_Out(str,10000);  //调试用;
		AfxMessageBox(str);
		Sleep(10000);
		*/
	for(i=3;i<15;i++)
	{
		if(m_nDiscardingCardsTable[i] == 1)   //遇到第一张单牌,接下去扫描;
		{
			m_nTypeValue = i;
			for(j=i;j<17;j++)    //从第i张牌开始扫描;
			{
				if(m_nDiscardingCardsTable[j] == 1)   //如果接下来的牌为单牌,则给连续单牌数加一;
				{
					counter++;
				}
				else if(m_nDiscardingCardsTable[j] == 0)//如果遇到不为单牌,
				{
					for(k=j;k<17;k++)   //如果之后遇到非空,则返回0;
					{
						if(m_nDiscardingCardsTable[k] != 0)
						{
							return 0;
						}
					}//end for(k=j+1;k<15;k++);

					if(counter >= 5)   //如果连续单牌>=5,则为单顺;
					{
						return m_nTypeValue;
					}
					else				//不为单牌,返回0
					{
						return 0;
					}
				}
				else      //不为空也不为单牌,返回0;
				{
					return 0;
				}
			}//
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;				  //遇到其他的数,返回
		}
	}
	/*
	char str[300];
			sprintf(str,"m_nDiscardingCardsTable[3] = %d\n\
				m_nDiscardingCardsTable[4] = %d\n\
				m_nDiscardingCardsTable[5] = %d\n\
				m_nDiscardingCardsTable[6] = %d\n\
				m_nDiscardingCardsTable[7] = %d\n\
				m_nDiscardingCardsTable[8] = %d\n\
				m_nDiscardingCardsTable[9] = %d\n\
				m_nDiscardingCardsTable[10] = %d\n\
				m_nDiscardingCardsTable[11] = %d\n\
				m_nDiscardingCardsTable[12] = %d\n\
				m_nDiscardingCardsTable[13] = %d\n\
				m_nDiscardingCardsTable[14] = %d\n\
				m_nTypeValue = %d",
						m_nDiscardingCardsTable[3],
						m_nDiscardingCardsTable[4],
						m_nDiscardingCardsTable[5],
						m_nDiscardingCardsTable[6],
						m_nDiscardingCardsTable[7],
						m_nDiscardingCardsTable[8],
						m_nDiscardingCardsTable[9],
						m_nDiscardingCardsTable[10],
						m_nDiscardingCardsTable[11],
						m_nDiscardingCardsTable[12],
						m_nDiscardingCardsTable[13],
						m_nDiscardingCardsTable[14],
						m_nTypeValue);
//			pMy_Draw->Text_Out(str,10000);  //调试用;
		AfxMessageBox(str);
		Sleep(10000);
*/
	return -1;   //见鬼了才会返回这个值;
}
//判断是否是双顺;此函数适合6-20张牌情况;
int CPlayingCards::IsDoubleSeries()
{
	int m_nTypeValue = 0;	//对牌的起始大小;
	int counter = 0;  //连续对牌的个数;
	int i,j,k;

	for(i=15; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}

	for(i=3; i<15; i++)
	{
		if(m_nDiscardingCardsTable[i] == 2)   //遇到第一个对牌,接下去扫描;
		{
			m_nTypeValue = i;
			for(j=i; j<15; j++)    //从第i张牌开始扫描;
			{
				if(m_nDiscardingCardsTable[j] == 2)   //如果接下来的牌为对牌,则给连续对牌数加一;
				{
					counter++;
				}
				else if(m_nDiscardingCardsTable[j] == 0)//如果遇到不为对牌,
				{
					for(k=j+1; k<15; k++)   //如果之后遇到非空,则返回0;
					{
						if(m_nDiscardingCardsTable[k] != 0)
						{
							return 0;
						}
					}//end for(k=j+1;k<15;k++);

					if(counter >= 3)   //如果连续对牌>=3,则为双顺;
					{
						return m_nTypeValue;
					}
					else				//不为对牌,返回0
					{
						return 0;
					}
				}
				else      //不为空也不为对牌,返回0;
				{
					return 0;
				}
			}//
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;				  //遇到其他的数,返回
		}
	}
	return -1;   //见鬼了才会返回这个值;
}
//判断是否是三顺;此函数适合6-18张牌情况;
int CPlayingCards::IsThreeSeries()
{
	int m_nTypeValue = 0;	//三顺的起始大小;
	int counter = 0;  //连续三张的个数;
	int i,j,k;

	for(i=15; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}

	for(i=3; i<15; i++)
	{
		if(m_nDiscardingCardsTable[i] == 3)   //遇到第一个三张,接下去扫描;
		{
			m_nTypeValue = i;
			for(j=i; j<15; j++)    //从表第i项开始扫描;
			{
				if(m_nDiscardingCardsTable[j] == 3)   //如果接下来的牌为三张,则给连续对牌数加一;
				{
					counter++;
				}
				else if(m_nDiscardingCardsTable[j] == 0)//如果遇到不为三张,
				{
					for(k=j+1;k<15;k++)   //如果之后遇到非空,则返回0;
					{
						if(m_nDiscardingCardsTable[k] != 0)
						{
							return 0;
						}
					}//end for(k=j+1;k<15;k++);

					if(counter >= 2)   //如果连续对牌>=2,则为三顺;
					{
						return m_nTypeValue;
					}
					else				//只有一个三张;
					{
						return 0;
					}
				}
				else      //不为空也不为对牌,返回0;
				{
					return 0;
				}
			}//
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;				  //遇到其他的数,返回
		}
	}
	return -1;
}
//判断是否是一对牌;
int CPlayingCards::Is2()
{
	for(int i=3;i<17;i++)
	{
		if(m_nDiscardingCardsTable[i] == 2)
		{
			return i;
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
	return -1;    //出错,为空牌表;
}
//判断是否是三张;
int CPlayingCards::Is3()
{
	for(int i=3; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] == 3)
		{
			return i;
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
	return -1;
}
//判断是否是四张(炸弹); 
int CPlayingCards::IsBomb()
{
	for(int i=3; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] == 4)
		{
			return i;
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
	return -1;
}
//判断是否是三带一单;
int CPlayingCards::Is31()
{
	for(int i=3; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] == 3)
			return i;
	}
	return 0;
}
//判断是否是三带一对;
int CPlayingCards::Is32()
{
	int m_nTypeValue = 0;
	int Pair = 0;
	for(int i=3; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] == 3)
		{
			m_nTypeValue = i;
		}
		else if(m_nDiscardingCardsTable[i] == 2)
		{
			Pair = i;
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
	//如果存在三张,又存在一对,则返回
	if(m_nTypeValue && Pair)
	{
		return m_nTypeValue;
	}
	return 0;
}
//判断是否是四带两单;
int CPlayingCards::Is411()
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] == 4)
		{
			m_nTypeValue = i;
		}
		else if(m_nDiscardingCardsTable[i] == 1)
		{
			counter++;
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
	//如果存在三张,又存在一对,则返回
	if(m_nTypeValue && counter == 2)
	{
		return m_nTypeValue;
	}
	
	return 0;
}
//判断是否是三顺带两单;
int CPlayingCards::Is3311()
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3; i<17; i++)
	{
		if((m_nDiscardingCardsTable[i] == 3) && (i!= 14))   //遇到第一个三张且不等于A;
		{
			m_nTypeValue = i;
			if(m_nDiscardingCardsTable[i+1] != 3)  
			{
				return 0;			//如果下表项不为三张,返回0;
			}
			else
			{
				for(int j=i+2; j<17; j++)
				{
					if(m_nDiscardingCardsTable[j] == 1)
					{
						counter++;
					}
					else if(m_nDiscardingCardsTable[j] != 0)
					{
						return 0;	//再出现其它表项,则返回0;
					}
				}//end for(int j=i+1;j<17;j++)
			}
			//如果单牌数为2,则匹配成功!
			if(counter == 2)
			{
				return m_nTypeValue;
			}
			else
			{
				return 0;
			}				
		}//
		else if(m_nDiscardingCardsTable[i] == 1)
		{
			counter++;
		}//
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}//
	}

	return 0;
}
//判断是否是四带两对;
int CPlayingCards::Is422()
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3;i<17;i++)
	{
		if(m_nDiscardingCardsTable[i] == 4)
		{
			m_nTypeValue = i;
		}
		else if(m_nDiscardingCardsTable[i] == 2)
		{
			counter++;
		}
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}
	}
	//如果存在三张,又存在一对,则返回
	if(m_nTypeValue && counter == 2)
	{
		return m_nTypeValue;
	}
	return 0;
}
//判断是否是三顺带两对;	
int CPlayingCards::Is3322()
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3; i<17; i++)
	{
		if((m_nDiscardingCardsTable[i] == 3) && (i < 14))   //遇到第一个三张且不等于A;
		{
			m_nTypeValue = i;
			if(m_nDiscardingCardsTable[i+1] != 3)  
			{
				return 0;			//如果下表项不为三张,返回0;
			}
			else
			{
				for(int j=i+2; j<17; j++)
				{
					if(m_nDiscardingCardsTable[j] == 2)
					{
						counter++;
					}
					else if(m_nDiscardingCardsTable[j] != 0)
					{
						return 0;	//再出现其它表项,则返回0;
					}
				}//end for(int j=i+1; j<17; j++)
			}
			//如果单牌数为2,则匹配成功!
			if(counter == 2)
			{
				return m_nTypeValue;
			}
			else
			{
				return 0;
			}				
		}//
		else if(m_nDiscardingCardsTable[i] == 2)
		{
			counter++;
		}//
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}//
	}

	return 0;
}
//判断是否是三顺带三单;
int CPlayingCards::Is333111()
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3; i<17; i++)
	{
		if((m_nDiscardingCardsTable[i] == 3) && (i < 13))   //遇到第一个三张且不等于A;
		{
			m_nTypeValue = i;
			if(m_nDiscardingCardsTable[i+1] != 3 || 
			   m_nDiscardingCardsTable[i+2] != 3)  
			{
				return 0;			//如果下表项不为三张,返回0;
			}
			else
			{
				for(int j=i+3; j<17; j++)
				{
					if(m_nDiscardingCardsTable[j] == 1)
					{
						counter++;
					}
					else if(m_nDiscardingCardsTable[j] != 0)
					{
						return 0;	//再出现其它表项,则返回0;
					}
				}//end for(int j=i+1;j<17;j++)
			}
			//如果单牌数为2,则匹配成功!
			if(counter == 3)
			{
				return m_nTypeValue;
			}
			else
			{
				return 0;
			}				
		}//
		else if(m_nDiscardingCardsTable[i] == 1)
		{
			counter++;
		}//
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}//
	}
	return 0;
}
//判断是否是三顺带三对;
int CPlayingCards::Is333222()	
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3; i<17; i++)
	{
		if(m_nDiscardingCardsTable[i] == 3 && i < 13)   //遇到第一个三张且不等于A;
		{
			m_nTypeValue = i;
			if((m_nDiscardingCardsTable[i+1] != 3) || 
			   (m_nDiscardingCardsTable[i+2] != 3))  
			{
				return 0;			//如果下表项不为三张,返回0;
			}
			else
			{
				for(int j=i+3; j<17; j++)
				{
					if(m_nDiscardingCardsTable[j] == 2)
					{
						counter++;
					}
					else if(m_nDiscardingCardsTable[j] != 0)
					{
						return 0;	//再出现其它表项,则返回0;
					}
				}//end for(int j=i+1;j<17;j++)
			}
			//如果单牌数为2,则匹配成功!
			if(counter == 3)
			{
				return m_nTypeValue;
			}
			else
			{
				return 0;
			}				
		}//
		else if(m_nDiscardingCardsTable[i] == 2)
		{
			counter++;
		}//
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}//
	}

	return 0;
}
//判断是否是三顺带四单;
int CPlayingCards::Is33331111()
{
	int m_nTypeValue = 0;
	int counter = 0;

	for(int i=3;i<17;i++)
	{
		if(m_nDiscardingCardsTable[i] == 3 && i < 12)   //遇到第一个三张且不等于A;
		{
			m_nTypeValue = i;
			if((m_nDiscardingCardsTable[i+1] != 3) || 
			   (m_nDiscardingCardsTable[i+2] != 3) ||
			   (m_nDiscardingCardsTable[i+3] != 3))  
			{
				return 0;			//如果下表项不为三张,返回0;
			}
			else
			{
				for(int j=i+4; j<17; j++)
				{
					if(m_nDiscardingCardsTable[j] == 1)
					{
						counter++;
					}
					else if(m_nDiscardingCardsTable[j] != 0)
					{
						return 0;	//再出现其它表项,则返回0;
					}
				}//end for(int j=i+1;j<17;j++)
			}
			//如果单牌数为2,则匹配成功!
			if(counter == 4)
			{
				return m_nTypeValue;
			}
			else
			{
				return 0;
			}				
		}//
		else if(m_nDiscardingCardsTable[i] == 1)
		{
			counter++;
		}//
		else if(m_nDiscardingCardsTable[i] != 0)
		{
			return 0;
		}//
	}
	return 0;
}
/*
* 函数介绍：检查出牌的逻辑合法性;
* 输入参数：无;
* 输出参数：??
* 返回值 ： 匹配成功返回1,不成功返回0;
*/
int CPlayingCards::CheckChoosing()
{
	int i;

	for(i=0; i<17; i++)    //初始化扫描表;
	{
		m_nDiscardingCardsTable[i] = 0;
	}
	//扫描进表中;
	for(i=0; i<m_nChoosingCardsCounter; i++)
	{
		m_nDiscardingCardsTable[m_cChoosingCards[i].m_nValue]++;
	}

	int Table_Index;

	switch(m_nChoosingCardsCounter)
	{
	case 1:
		m_cDiscardingType.m_nTypeNum   = 1;
		m_cDiscardingType.m_nTypeValue = m_cChoosingCards[0].m_nValue; 
		return 1;
	case 2://是否是一对牌;
		if( (Table_Index = Is2()) != 0 )
		{
			//是大王,则为炸弹;
			if(Table_Index == 16)
			{
				m_cDiscardingType.m_nTypeNum = 4;   //类型为4表示炸弹;
			}
			else
			{
				m_cDiscardingType.m_nTypeNum = 2;
			}
			m_cDiscardingType.m_nTypeValue = Table_Index; 
			return 1;
		}
		return 0;
	case 3:
		if( (Table_Index = Is3()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum   = 3;
			m_cDiscardingType.m_nTypeValue = Table_Index; 
			return 1;
		}
		return 0;
	case 4:
		if( (Table_Index = IsBomb()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum   = 4;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is31()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 31;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 5:
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 5;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is32()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 32;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 6:
//单顺:		 6							最小牌的Card.m_nValue(面值)    6
//双顺:		 222						最小牌的Card.m_nValue(面值)    6
//三顺:	     33							最小牌的Card.m_nValue(面值)    6
//四带二单:	 411						四张的Card.m_nValue(面值)		6	 
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 6;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsThreeSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 33;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is411()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 411;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 7:
//单顺
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 7;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 8:
//单顺:		 8							最小牌的Card.m_nValue(面值)    8
//双顺		 2222						最小牌的Card.m_nValue(面值)    8
//三顺带二单:  3311						最小三张的Card.m_nValue(面值)  8
//四带二对:	 422						四张的Card.m_nValue(面值)		8
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 8;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 2222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is3311()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 3311;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is422()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 422;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 9:
//单顺:		 9							最小牌的Card.m_nValue(面值)    9
//三顺:		 333						最小三张的Card.m_nValue(面值)  9
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 9;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsThreeSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 333;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 10:
//单顺:		 10							最小牌的Card.m_nValue(面值)    10
//双顺:		 22222						最小牌的Card.m_nValue(面值)    10
//三顺带二对:  3322						最小三张的Card.m_nValue(面值)  10
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 10;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 22222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is3322()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 3322;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 11:
//单顺:		 11							最小牌的Card.m_nValue(面值)    11	
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 11;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 12:
//单顺:		 12							最小牌的Card.m_nValue(面值)    12
//双顺:		 222222						最小对牌的Card.m_nValue(面值)  12
//三顺:		 3333						最小三张的Card.m_nValue(面值)  12
//三顺带三:	 333111						最小三张的Card.m_nValue(面值)  12
		if( (Table_Index = IsSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 12;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 222222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsThreeSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 3333;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is333111()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 333111;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 13:
		return 0;
	case 14:
//双顺		 2222222					最小对牌的Card.m_nValue(面值)  14
		if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 2222222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 15:
//三顺带三对:  333222					最小三张的Card.m_nValue(面值)  15
//三顺:		 33333						最小三张的Card.m_nValue(面值)  15
		if( (Table_Index = IsThreeSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 33333;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is333222()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 333222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 16:
//双顺		 22222222					最小对牌的Card.m_nValue(面值)  16
//三顺带四单:  33331111					最小三张的Card.m_nValue(面值)  16
		if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 22222222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = Is33331111()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 33331111;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 17:
		return 0;
	case 18:
//双顺		 222222222					最小对牌的Card.m_nValue(面值)  18
//三顺		 333333						最小三张的Card.m_nValue(面值)  18
		if( (Table_Index = IsDoubleSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 222222222;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		else if( (Table_Index = IsThreeSeries()) != 0 )
		{
			m_cDiscardingType.m_nTypeNum = 333333;
			m_cDiscardingType.m_nTypeValue = Table_Index;
			return 1;
		}
		return 0;
	case 19:
		return 0;
	case 20:
		return 0;
//暂时保留;几乎不可能的牌;
//双顺		 2222222222					最小对牌的Card.m_nValue(面值)  20
//三顺带五单:	 3333311111					最小三张的Card.m_nValue(面值)  20
//三顺带四对:  33332222					最小三张的Card.m_nValue(面值)  20
	}	
	return -1;
}
//直接选择出牌;
int CPlayingCards::DirectChoose()
{	
	int i;
	int counter_0 = 0;
	int counter_1 = 0;
	int value_1 = 17;
	int counter_2 = 0;
	int value_2 = 17;
	int counter_3 = 0;
	int value_3 = 17;
	int counter_4 = 0;
	int value_4 = 17;

	CCardsType temp;
	temp.m_nTypeValue = 2;
	//扫描未出牌表;
	for(i=15;i>=3;i--)
	{
		switch(m_nCardsTable[i])
		{
		case 0:
			counter_0++;
			break;
		case 1:
			counter_1++;
			value_1 = i;
			break;
		case 2:
			counter_2++;
			value_2 = i;
			break;
		case 3:
			counter_3++;
			value_3 = i;
			break;
		case 4:
			counter_4++;
			value_4 = i;
			break;
		}
	}

		if( counter_3 >= 2 && counter_1 >= counter_2 )
		{
			//寻找3311;找到返回;
			//如果没找到,则 if(counter_1 <= 5)寻找31;返回
			if( Search3311(temp) )
			{
				return 1;
			}
		}
		else if( counter_3 >= 2 && counter_2 > counter_1 )
		{
			//寻找3322;找到返回;
			//如果没找到,则 if(counter_2 <= 5)寻找32;返回
			if( Search3322(temp) )
			{
				return 1;
			}
		}

		if( counter_1 >= 3 )
		{
			//寻找单顺;从12张到5张的情况;

			if( SearchBeyond10(temp,12) )
				return 1;

			if( SearchBeyond10(temp,11) )
				return 1;

			if( SearchBeyond10(temp,10) )
				return 1;

			if( Search9(temp) )
				return 1;
				
			if( Search8(temp) )
				return 1;

			if( Search7(temp) )
				return 1;

			if( Search6(temp) )
				return 1;

			if( Search5(temp) )
				return 1;		
		}

		if( counter_2 >= 3 )
		{
			//寻找双顺;从5到3;
			if( Search2222(temp) )
				return 1;

			if( Search222(temp) )
				return 1;
		}

		if( counter_4 >= 0 && counter_1 >= 2 && counter_2 >= 2 )
		{
			//寻找411;
			if( value_1 < value_2 )
			{		
				if( Search411(temp) )
				return 1;
			}
			else
			{
				if( Search422(temp) )
				return 1;
			}
		}
		else if( counter_4 >= 0 && counter_1 >= 2 )
		{
			if( Search411(temp) )
				return 1;
		}
		else if( counter_4 >= 0 && counter_2 >= 2 )
		{
			if( Search422(temp) )
				return 1;
		}

		if( counter_3 > 0 && value_1 < value_2 && counter_1 > 0 )
		{
			//寻找31;
			if( Search31(temp) )
				return 1;
		}
		else if( counter_3 > 0 && value_2 < value_1 && counter_2 > 0 )
		{
			//寻找31;
			if( Search32(temp) )
				return 1;
		}

		if( value_1 < value_2 )
		{
			//寻找1;
			if( Search1(temp) )
				return 1;
		}
		else
		{
			//寻找2;
			if( Search2(temp) )
				return 1;
		}

	for(i=3;i<=16;i++)
	{
		if( m_nCardsTable[i] == 0 )
			continue;
		else
		{
			m_cDiscardingType.m_nTypeNum   = 4;
			m_cDiscardingType.m_nTypeNum   = m_nCardsTable[i];
			m_cDiscardingType.m_nTypeValue = i;

			for(int j=0;j<m_nCardsTable[i];j++)
			{
				m_nDiscardingCounter++;
				m_cDiscarding[j] = m_cCards[j];

				m_cCards[j] = m_cBLANK_CARD;
				}//end for~
			m_nCardsTable[i] = 0;
			break;
		}
	}//end if

//	CleanUp();
	return 1;
}
//工人智能补丁1;
BOOL CPlayingCards::AI_Patch1()
{
	//双王
	if( (m_nCardsTable[16] == 2) && (m_nCardsCounter == 2) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;
		m_nDiscardingCounter = 2;

		m_cDiscarding[0]  = m_cCards[0];
		m_cDiscarding[1]  = m_cCards[1];

		m_cCards[0] = m_cBLANK_CARD;
		m_cCards[1] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	int i;
	int counter_1 = 0;
	int value_1 = 17;
	int counter_2 = 0;
	int value_2 = 17;
	int counter_3 = 0;
	int value_3 = 17;
	int counter_4 = 0;
	int value_4 = 17;

	CCardsType temp;
	temp.m_nTypeValue = 2;
	//扫描未出牌表;
	for(i=15;i>=3;i--)
	{
		switch(m_nCardsTable[i])
		{
		case 1:
			counter_1++;
			value_1 = i;
			break;
		case 2:
			counter_2++;
			value_2 = i;
			break;
		case 3:
			counter_3++;
			value_3 = i;
			break;
		case 4:
			counter_4++;
			value_4 = i;
			break;
		}
	}
	//双王加单张;
	if( (m_nCardsTable[16] == 2) && (m_nCardsCounter == 3) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[1];
		m_cDiscarding[1]     = m_cCards[2];

		m_cCards[1] = m_cBLANK_CARD;
		m_cCards[2] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	//双王加一对;
	if( (m_nCardsTable[16] == 2) && ((m_nCardsCounter == 4) && (counter_2 == 1)) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[2];
		m_cDiscarding[1]     = m_cCards[3];

		m_cCards[2] = m_cBLANK_CARD;
		m_cCards[3] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	//双王加三张;
	if( (m_nCardsTable[16] == 2) && ((m_nCardsCounter == 5) && (counter_3 == 1)) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[3];
		m_cDiscarding[1]     = m_cCards[4];

		m_cCards[3] = m_cBLANK_CARD;
		m_cCards[4] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}
	//双王加四张
	if( (m_nCardsTable[16] == 2) && ((m_nCardsCounter == 6) && (counter_4 == 1)) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[4];
		m_cDiscarding[1]     = m_cCards[5];

		m_cCards[4] = m_cBLANK_CARD;
		m_cCards[5] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	//双王加31
	if( (m_nCardsTable[16] == 2) && 
		((m_nCardsCounter == 6) && (counter_3 == 1)) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[4];
		m_cDiscarding[1]     = m_cCards[5];

		m_cCards[4] = m_cBLANK_CARD;
		m_cCards[5] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	//双王加32
	if( (m_nCardsTable[16] == 2) && 
		((m_nCardsCounter == 7) && (counter_3 == 1 && counter_2 == 1)) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[5];
		m_cDiscarding[1]     = m_cCards[6];

		m_cCards[5] = m_cBLANK_CARD;
		m_cCards[6] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	//双王加42
	if( (m_nCardsTable[16] == 2) && 
		((m_nCardsCounter == 8) && (counter_4 == 1 && counter_1 == 2)) )
	{
		m_cDiscardingType.m_nTypeNum   = 4;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter = 2;
		m_cDiscarding[0]     = m_cCards[6];
		m_cDiscarding[1]     = m_cCards[7];

		m_cCards[6] = m_cBLANK_CARD;
		m_cCards[7] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;
		return 1;
	}

	return 0;
}
//寻找单牌出;
int CPlayingCards::Search1(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}

	int i,j;
	int counter_0 = 0;
	int counter_1 = 0;
	int counter_2 = 0;
	int counter_3 = 0;
	int counter_4 = 0;

	//扫描未出牌表;
	for(i=3;i<15;i++)
	{
		switch(m_nCardsTable[i])
		{
		case 0:
			counter_0++;
			break;
		case 1:
			counter_1++;
			break;
		case 2:
			counter_2++;
			break;
		case 3:
			counter_3++;
			break;
		case 4:
			counter_4++;
			break;
		}
	}
	//补丁:
	if( Cards_Type.m_nTypeValue == 16 && counter_1 > 3 )
	{
		return 0;
	}

	if( m_nCardsTable[16] == 1 
		&& m_cCards[m_nCardsCounter - 1].m_nColor == 1 
		&& Cards_Type.m_nTypeValue == 16 )  //大王,可以出;
	{
		m_cDiscardingType.m_nTypeNum   = 1;
		m_cDiscardingType.m_nTypeValue = 16;

		m_nDiscardingCounter++;
		m_cDiscarding[0] = m_cCards[m_nCardsCounter - 1];
		
		m_cCards[m_nCardsCounter - 1] = m_cBLANK_CARD;

		m_nCardsTable[16] = 0;

		return 1;
	}

	//
	for(i=Cards_Type.m_nTypeValue+1;i<=16;i++)
	{
		if( m_nCardsTable[i] == 1)
		{
			m_cDiscardingType.m_nTypeNum   = 1;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter++;
					m_cDiscarding[0] = m_cCards[j];

					m_cCards[j] = m_cBLANK_CARD;

					m_nCardsTable[i] = 0;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~
	//撤两张的;
	for(i=16;i>=Cards_Type.m_nTypeValue+1;i--)
	{
		if( m_nCardsTable[i] == 2 )
		{
			m_cDiscardingType.m_nTypeNum   = 1;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter++;
					m_cDiscarding[0] = m_cCards[j];

					m_cCards[j] = m_cBLANK_CARD;

					m_nCardsTable[i]--;  //应该等于1;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~
	//撤三张的;
	for(i=15;i>=Cards_Type.m_nTypeValue+1;i--)
	{
		if( m_nCardsTable[i] == 3 )
		{
			m_cDiscardingType.m_nTypeNum   = 1;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter++;
					m_cDiscarding[0] = m_cCards[j];

					m_cCards[j] = m_cBLANK_CARD;

					m_nCardsTable[i]--;  //应该等于1;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~

	return 0;
}
//寻找对牌出;
int CPlayingCards::Search2(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}

	int i,j;
	for(i=Cards_Type.m_nTypeValue+1;i<16;i++)
	{
		if( m_nCardsTable[i] == 2 )
		{
			m_cDiscardingType.m_nTypeNum   = 2;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter += 2;
					m_cDiscarding[0] = m_cCards[j];
					m_cDiscarding[1] = m_cCards[j + 1];
		
					m_cCards[j] = m_cBLANK_CARD;
					m_cCards[j + 1] = m_cBLANK_CARD;

					m_nCardsTable[i] = 0;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~
	//撤三张的;
if( Cards_Type.m_nTypeValue >= 12 )   //如果是比较大的牌,才考虑拆牌,而且2不会拆;
{ 
	for(i=14;i>=Cards_Type.m_nTypeValue+1;i--)
	{
		if( m_nCardsTable[i] == 3 )
		{
			m_cDiscardingType.m_nTypeNum   = 2;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter += 2;
					m_cDiscarding[0] = m_cCards[j];
					m_cDiscarding[1] = m_cCards[j + 1];

					m_cCards[j] = m_cBLANK_CARD;
					m_cCards[j + 1] = m_cBLANK_CARD;

					m_nCardsTable[i] -= 2;  //应该等于1;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~
}
	return 0;
}
//寻找三牌出;
int CPlayingCards::Search3(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//三张;
	int i,j;
	for(i=Cards_Type.m_nTypeValue+1;i<16;i++)
	{
		if( m_nCardsTable[i] == 3)
		{
			m_cDiscardingType.m_nTypeNum   = 3;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter += 3;
					m_cDiscarding[0] = m_cCards[j];
					m_cDiscarding[1] = m_cCards[j + 1];
					m_cDiscarding[2] = m_cCards[j + 2];
		
					m_cCards[j + 0] = m_cBLANK_CARD;
					m_cCards[j + 1] = m_cBLANK_CARD;
					m_cCards[j + 2] = m_cBLANK_CARD;

					m_nCardsTable[i] = 0;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~
	return 0;
}
//寻找炸弹出;
int CPlayingCards::SearchBomb(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//四张;
	//注意加分!!!;
	int i,j;
	for(i=Cards_Type.m_nTypeValue+1;i<16;i++)
	{
		if( m_nCardsTable[i] == 4 )
		{
			m_cDiscardingType.m_nTypeNum   = 4;
			m_cDiscardingType.m_nTypeValue = i;

			for(j=0;j<m_nCardsCounter;j++)
			{
				if( m_cCards[j].m_nValue == i )
				{
					m_nDiscardingCounter += 4;
					m_cDiscarding[0] = m_cCards[j];
					m_cDiscarding[1] = m_cCards[j + 1];
					m_cDiscarding[2] = m_cCards[j + 2];
					m_cDiscarding[3] = m_cCards[j + 3];
		
					m_cCards[j + 0] = m_cBLANK_CARD;
					m_cCards[j + 1] = m_cBLANK_CARD;
					m_cCards[j + 2] = m_cBLANK_CARD;
					m_cCards[j + 3] = m_cBLANK_CARD;
	
					m_nCardsTable[i] = 0;
					return 1;
				}
			}//end for~~
		}//end if~
	} //end for~
	return 0;
}
//寻找三带2出;
int CPlayingCards::Search31(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//三张;
	int i,j,k;
	//补丁
	int nMax = 15;
	if( m_nCardsCounter <= 5 )
	{
		nMax = 16;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<nMax;i++)   //三张2一般不出;
	{
		if( m_nCardsTable[i] == 3 )
		{
			for(j=3;j<=15;j++)
			{
				if( m_nCardsTable[j] == 1 )
				{
					m_cDiscardingType.m_nTypeNum   = 31;
					m_cDiscardingType.m_nTypeValue = i;
					m_nDiscardingCounter = 4;
					//三张的;
					for(k=0;k<m_nCardsCounter;k++)
					{
						if( m_cCards[k].m_nValue == i )
						{
							m_cDiscarding[0] = m_cCards[k];
							m_cDiscarding[1] = m_cCards[k + 1];
							m_cDiscarding[2] = m_cCards[k + 2];
		
							m_cCards[k + 0] = m_cBLANK_CARD;
							m_cCards[k + 1] = m_cBLANK_CARD;
							m_cCards[k + 2] = m_cBLANK_CARD;

							m_nCardsTable[i] = 0;
						}
					}//end for~~
					//单张的;
					for(k=0;k<m_nCardsCounter;k++)
					{
						if( m_cCards[k].m_nValue == j )
						{
							m_cDiscarding[3] = m_cCards[k];

							m_cCards[k] = m_cBLANK_CARD;

							m_nCardsTable[j] = 0;
							return 1;
						}
					}//end for~~

				}//end if~
			} //end for~
			return 0;
		}//end if~
	} //end for~
	return 0;
}
//寻找三带2出; 
int CPlayingCards::Search32(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//三张;
	int i,j,k;

	int nMax = 15;
	if( m_nCardsCounter <= 6 )
	{
		nMax = 16;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<nMax;i++)   //三张2一般不出;
	{
		if( m_nCardsTable[i] == 3 )
		{
			for(j=3; j<=14; j++)
			{
				if( m_nCardsTable[j] == 2 )
				{
					m_cDiscardingType.m_nTypeNum   = 32;
					m_cDiscardingType.m_nTypeValue = i;
					m_nDiscardingCounter = 5;
					//三张的;
					for(k=0;k<m_nCardsCounter;k++)
					{
						if( m_cCards[k].m_nValue == i )
						{
							m_cDiscarding[0] = m_cCards[k];
							m_cDiscarding[1] = m_cCards[k + 1];
							m_cDiscarding[2] = m_cCards[k + 2];
		
							m_cCards[k + 0] = m_cBLANK_CARD;
							m_cCards[k + 1] = m_cBLANK_CARD;
							m_cCards[k + 2] = m_cBLANK_CARD;

							m_nCardsTable[i] = 0;
						}
					}//end for~~
					//单张的;
					for(k=0; k<m_nCardsCounter; k++)
					{
						if( m_cCards[k].m_nValue == j )
						{
							m_cDiscarding[3] = m_cCards[k];
							m_cDiscarding[4] = m_cCards[k + 1];

							m_cCards[k + 0] = m_cBLANK_CARD;
							m_cCards[k + 1] = m_cBLANK_CARD;

							m_nCardsTable[j] = 0;
							return 1;
						}
					}//end for~~
				}//end if~
			} //end for~
			return 0;
		}//end if~
	} //end for~
	return 0;
}
//寻找四带2单;
int CPlayingCards::Search411(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//四张;
	int i,j,k,l;

	for(i=Cards_Type.m_nTypeValue+1;i<16;i++)
	{
		if( m_nCardsTable[i] == 4)
		{
			for(j=3; j<=14; j++)
			{
				if( m_nCardsTable[j] == 1 )
				{
					//扫描第二张单牌;
					for(k=j+1;k<=14;k++)
					{
						if( m_nCardsTable[k] == 1 )
						{
							m_cDiscardingType.m_nTypeNum   = 411;
							m_cDiscardingType.m_nTypeValue = i;
							m_nDiscardingCounter = 6;

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == i )
								{
									m_cDiscarding[0] = m_cCards[l];
									m_cDiscarding[1] = m_cCards[l + 1];
									m_cDiscarding[2] = m_cCards[l + 2];
									m_cDiscarding[3] = m_cCards[l + 3];
		
									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;
									m_cCards[l + 2] = m_cBLANK_CARD;
									m_cCards[l + 3] = m_cBLANK_CARD;

									m_nCardsTable[i] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == j )
								{	
									m_cDiscarding[4] = m_cCards[l];
		
									m_cCards[l] = m_cBLANK_CARD;

									m_nCardsTable[j] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == k )
								{
									m_cDiscarding[5] = m_cCards[l];

									m_cCards[l] = m_cBLANK_CARD;

									m_nCardsTable[k] = 0;
								}
							}
							return 1;
						}//end if~
					} //end for~
					return 0;
				}//end if~
			} //end for~
			return 0;
		}
	}
	return 0;
}
//寻找四带两对;
int CPlayingCards::Search422(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//四张;
	int i,j,k,l;

	for(i=Cards_Type.m_nTypeValue+1;i<16;i++)
	{
		if( m_nCardsTable[i] == 4 )
		{
			for(j=3;j<=14;j++)
			{
				if( m_nCardsTable[j] == 2 )
				{
					//扫描第二张单牌;
					for(k=j+1;k<=14;k++)
					{
						if( m_nCardsTable[k] == 2 )
						{
							m_cDiscardingType.m_nTypeNum   = 422;
							m_cDiscardingType.m_nTypeValue = i;
							m_nDiscardingCounter = 8;

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == i )
								{
									m_cDiscarding[0] = m_cCards[l];
									m_cDiscarding[1] = m_cCards[l + 1];
									m_cDiscarding[2] = m_cCards[l + 2];
									m_cDiscarding[3] = m_cCards[l + 3];
		
									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;
									m_cCards[l + 2] = m_cBLANK_CARD;
									m_cCards[l + 3] = m_cBLANK_CARD;

									m_nCardsTable[i] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == j )
								{	
									m_cDiscarding[4] = m_cCards[l];
									m_cDiscarding[5] = m_cCards[l + 1];
		
									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;

									m_nCardsTable[j] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == k )
								{
									m_cDiscarding[6] = m_cCards[l];
									m_cDiscarding[7] = m_cCards[l + 1];

									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;

									m_nCardsTable[k] = 0;
								}
							}
							return 1;
						}//end if~
					} //end for~
					return 0;
				}//end if~
			} //end for~
			return 0;
		}
	}
	return 0;
}
//寻找三顺带两单;
int CPlayingCards::Search3311(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//三张;
	int i,j,k,l;
	for(i=Cards_Type.m_nTypeValue+1;i<14;i++)   //2一般不能出在顺子中;
	{
		if( m_nCardsTable[i] == 3 && m_nCardsTable[i + 1] == 3 )
		{
			for(j=3;j<=14;j++)
			{
				if( m_nCardsTable[j] == 1 )
				{
					//扫描第二张单牌;
					for(k=j+1;k<=14;k++)
					{
						if( m_nCardsTable[k] == 1 )
						{
							m_cDiscardingType.m_nTypeNum   = 3311;
							m_cDiscardingType.m_nTypeValue = i;
							m_nDiscardingCounter = 8;

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == i )
								{
									m_cDiscarding[0] = m_cCards[l];
									m_cDiscarding[1] = m_cCards[l + 1];
									m_cDiscarding[2] = m_cCards[l + 2];
									m_cDiscarding[3] = m_cCards[l + 3];
									m_cDiscarding[4] = m_cCards[l + 4];
									m_cDiscarding[5] = m_cCards[l + 5];
									
									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;
									m_cCards[l + 2] = m_cBLANK_CARD;
									m_cCards[l + 3] = m_cBLANK_CARD;
									m_cCards[l + 4] = m_cBLANK_CARD;
									m_cCards[l + 5] = m_cBLANK_CARD;

									m_nCardsTable[i] = 0;
									m_nCardsTable[i + 1] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == j )
								{	
									m_cDiscarding[6] = m_cCards[l];
		
									m_cCards[l + 0] = m_cBLANK_CARD;

									m_nCardsTable[j] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == k )
								{
									m_cDiscarding[7] = m_cCards[l];

									m_cCards[l + 0] = m_cBLANK_CARD;

									m_nCardsTable[k] = 0;
								}
							}
							return 1;

						}//end if~
					} //end for~
					return 0;
				}//end if~
			}
			return 0;
		}
	}
	return 0;
}
//寻找三顺带两对;
int CPlayingCards::Search3322(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	//三张;
	int i,j,k,l;
	for(i=Cards_Type.m_nTypeValue+1;i<14;i++)   //2一般不能出在顺子中;
	{
		if( m_nCardsTable[i] == 3 && m_nCardsTable[i + 1] == 3 )
		{
			for(j=3;j<=14;j++)
			{
				if( m_nCardsTable[j] == 2 )
				{
					//扫描第二张单牌;
					for(k=j+1;k<=14;k++)
					{
						if( m_nCardsTable[k] == 2 )
						{
							m_cDiscardingType.m_nTypeNum   = 3322;
							m_cDiscardingType.m_nTypeValue = i;
							m_nDiscardingCounter = 10;

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == i )
								{
									m_cDiscarding[0] = m_cCards[l];
									m_cDiscarding[1] = m_cCards[l + 1];
									m_cDiscarding[2] = m_cCards[l + 2];
									m_cDiscarding[3] = m_cCards[l + 3];
									m_cDiscarding[4] = m_cCards[l + 4];
									m_cDiscarding[5] = m_cCards[l + 5];
									
									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;
									m_cCards[l + 2] = m_cBLANK_CARD;
									m_cCards[l + 3] = m_cBLANK_CARD;
									m_cCards[l + 4] = m_cBLANK_CARD;
									m_cCards[l + 5] = m_cBLANK_CARD;

									m_nCardsTable[i] = 0;
									m_nCardsTable[i + 1] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == j )
								{	
									m_cDiscarding[6] = m_cCards[l];
									m_cDiscarding[7] = m_cCards[l + 1];
									
									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;

									m_nCardsTable[j] = 0;
								}
							}//end for~~

							for(l=0;l<m_nCardsCounter;l++)
							{
								if( m_cCards[l].m_nValue == k )
								{
									m_cDiscarding[8] = m_cCards[l];
									m_cDiscarding[9] = m_cCards[l + 1];

									m_cCards[l + 0] = m_cBLANK_CARD;
									m_cCards[l + 1] = m_cBLANK_CARD;

									m_nCardsTable[k] = 0;
								}
							}
							return 1;

						}//end if~
					} //end for~
					return 0;
				}//end if~
			}
			return 0;
		}
	}
	return 0;
}

//寻找双顺;
int CPlayingCards::Search222(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}

	int i,j;
	//如果大于J,Q,K,则不要;
	if( Cards_Type.m_nTypeValue > 11 )
	{
		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=12;i++)
	{
		if( m_nCardsTable[i] == 2 )
		{
			if( m_nCardsTable[i + 1] == 2 && m_nCardsTable[i + 2] == 2 )
			{
				for(j=0;j<m_nCardsCounter;j++)
				{
					if( m_cCards[j].m_nValue == i )
					{
						m_cDiscardingType.m_nTypeNum   = 222;
						m_cDiscardingType.m_nTypeValue = i;

						m_nDiscardingCounter = 6;

						for(int k=0;k<6;k++)
						{
							m_cDiscarding[k] = m_cCards[j + k];				
							m_cCards[j + k] = m_cBLANK_CARD;
						}
						m_nCardsTable[i] = 0;
						m_nCardsTable[i + 1] = 0;
						m_nCardsTable[i + 2] = 0;
						return 1;
					}
				}//end for~~	
			}
		}//end if;
	}
	return 0;
}
//寻找双顺;
int CPlayingCards::Search2222(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}

	int i,j;
	//如果大于J,Q,K,则不要;
	if( Cards_Type.m_nTypeValue > 10 )
	{
		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=11;i++)
	{
		if( m_nCardsTable[i] == 2 )
		{
			if( m_nCardsTable[i + 1] == 2 && 
				m_nCardsTable[i + 2] == 2 && 
				m_nCardsTable[i + 3] == 2 )
			{
				for(j=0;j<m_nCardsCounter;j++)
				{
					if( m_cCards[j].m_nValue == i )
					{
						m_cDiscardingType.m_nTypeNum   = 2222;
						m_cDiscardingType.m_nTypeValue = i;

						m_nDiscardingCounter = 8;

						for(int k=0;k<8;k++)
						{
							m_cDiscarding[k] = m_cCards[j + k];				
							m_cCards[j + k] = m_cBLANK_CARD;
						}
						m_nCardsTable[i] = 0;
						m_nCardsTable[i + 1] = 0;
						m_nCardsTable[i + 2] = 0;
						m_nCardsTable[i + 3] = 0;
						return 1;
					}
				}//end for~~	
			}
		}//end if;
	}
	return 0;
}
//寻找5单顺出;
int CPlayingCards::Search5(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}

	int i,j,k,l;
	//连续单牌的个数;
	int counter_1;
	int counter_2;
	int counter_3;

	//如果是10,j,q,k,A,则考虑对手中牌的个数,如果太少就考虑寻找炸弹;

	if( Cards_Type.m_nTypeValue >= 10 ) 
	{
		return 0;
	}
	//第一遍扫描,看是否有全单的单顺;

	for(i=Cards_Type.m_nTypeValue+1;i<=10;i++)
	{
	    counter_1 = 0;
		for(j=i;j<i+5;j++)
		{
			if( m_nCardsTable[j] != 1 )
			{
				break;
			}
			else
			{
				counter_1++;
			}
		}
		//看连续牌数是否等于5;
		if( counter_1 == 5 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 5;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 5;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<5;l++)
					{
						m_cDiscarding[l] = m_cCards[k + l];
				
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l] = 0;
					}
				}				
			}
			return 1; //搞定,返回;
		}

		//第二遍扫描;
	    counter_1 = 0;
		counter_2 = 0;
		counter_3 = 0;

		for(j=i;j<i+5;j++)
		{
			if( m_nCardsTable[j] == 1 )
			{
				counter_1++;
			}
			else if( m_nCardsTable[j] == 2 )
			{
				counter_2++;
			}
			else if( m_nCardsTable[j] == 3 )
			{
				counter_3++;
			}
			else
			{
				break;  //如果没有牌或者是四张(炸弹),则不考虑;
			}
		}
		//看牌是否等于5;
		if( counter_1 == 4 && counter_2 + counter_3 == 1 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 5;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 5;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<5;l++)
					{
						if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
						{
							k++;
							if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
							{
								k++;
							}
						}

						m_cDiscarding[l] = m_cCards[k + l];
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l]--;
					}
				}				
			}
			return 1; //搞定,返回;
		}
	}//end for~;
	return 0;	
}
//寻找6单顺出;
int CPlayingCards::Search6(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}

	int i,j,k,l;
	//连续单牌的个数;
	int counter_1;
	int counter_2;
	int counter_3;

	//如果是10,j,q,k,A,则考虑对手中牌的个数,如果太少就考虑寻找炸弹;

	if( Cards_Type.m_nTypeValue >= 9 ) 
	{
		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=9;i++)
	{
		//第一遍扫描,看是否有全单的单顺;
	    counter_1 = 0;
		for(j=i;j<i+6;j++)
		{
			if( m_nCardsTable[j] != 1 )
			{
				break;
			}
			else
			{
				counter_1++;
			}
		}
		//看连续牌数是否等于6;
		if( counter_1 == 6 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 6;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 6;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<6;l++)
					{
						m_cDiscarding[l] = m_cCards[k + l];
				
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l] = 0;
					}
				}				
			}
			return 1; //搞定,返回;
		}

		//第二遍扫描;
	    counter_1 = 0;
		counter_2 = 0;
		counter_3 = 0;

		for(j=i;j<i+6;j++)
		{
			if( m_nCardsTable[j] == 1 )
			{
				counter_1++;
			}
			else if( m_nCardsTable[j] == 2 )
			{
				counter_2++;
			}
			else if( m_nCardsTable[j] == 3 )
			{
				counter_3++;
			}
			else
			{
				break;  //如果没有牌或者是四张(炸弹),则不考虑;
			}
		}
		//看牌是否等于5;
		if( counter_1 == 4 && counter_2 + counter_3 == 2 ||
			counter_1 == 5 && counter_2 + counter_3 == 1 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 6;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 6;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<6;l++)
					{
						if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
						{
							k++;
							if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
							{
								k++;
							}
						}

						m_cDiscarding[l] = m_cCards[k + l];
						m_cCards[k + l]  = m_cBLANK_CARD;
	
						m_nCardsTable[i + l]--;
					}
				}				
			}
			return 1; //搞定,返回;
		}
	}//end for~;
	return 0;
}
//寻找7单顺出;
int CPlayingCards::Search7(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	int i,j,k,l;
	//连续单牌的个数;
	int counter_1;
	int counter_2;
	int counter_3;

	//如果是10,j,q,k,A,则考虑对手中牌的个数,如果太少就考虑寻找炸弹;

	if( Cards_Type.m_nTypeValue >= 8 ) 
	{
		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=8;i++)
	{
		//第一遍扫描,看是否有全单的单顺;
	    counter_1 = 0;
		for(j=i;j<i+7;j++)
		{
			if( m_nCardsTable[j] != 1 )
			{
				break;
			}
			else
			{
				counter_1++;
			}
		}
		//看连续牌数是否等于7;
		if( counter_1 == 7 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 7;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 7;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<7;l++)
					{
						m_cDiscarding[l] = m_cCards[k + l];
				
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l] = 0;
					}
				}				
			}
			return 1; //搞定,返回;
		}

		//第二遍扫描;
	    counter_1 = 0;
		counter_2 = 0;
		counter_3 = 0;

		for(j=i;j<i+7;j++)
		{
			if( m_nCardsTable[j] == 1 )
			{
				counter_1++;
			}
			else if( m_nCardsTable[j] == 2 )
			{
				counter_2++;
			}
			else if( m_nCardsTable[j] == 3 )
			{
				counter_3++;
			}
			else
			{
				break;  //如果没有牌或者是四张(炸弹),则不考虑;
			}
		}
		//看连续牌数是否等于7;
		if( counter_1 == 4 && counter_2 + counter_3 == 3 ||
			counter_1 == 5 && counter_2 + counter_3 == 2 ||
			counter_1 == 6 && counter_2 + counter_3 == 1 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 7;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 7;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<7;l++)
					{
						if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
						{
							k++;
							if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
							{
								k++;
							}
						}

						m_cDiscarding[l] = m_cCards[k + l];
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l]--;
					}
				}				
			}
			return 1; //搞定,返回;
		}
	}//end for~;
	return 0;
}
//寻找8单顺出;
int CPlayingCards::Search8(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	int i,j,k,l;
	//连续单牌的个数;
	int counter_1;
	int counter_2;
	int counter_3;

	//如果是10,j,q,k,A,则考虑对手中牌的个数,如果太少就考虑寻找炸弹;

	if( Cards_Type.m_nTypeValue >= 7 ) 
	{
		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=7;i++)
	{
		//第一遍扫描,看是否有全单的单顺;
	    counter_1 = 0;
		for(j=i;j<i+8;j++)
		{
			if( m_nCardsTable[j] != 1 )
			{
				break;
			}
			else
			{
				counter_1++;
			}
		}
		//看连续牌数是否等于8;
		if( counter_1 == 8 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 8;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 8;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<8;l++)
					{
						m_cDiscarding[l] = m_cCards[k + l];
				
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l] = 0;
					}
				}				
			}
			return 1; //搞定,返回;
		}

		//第二遍扫描;
	    counter_1 = 0;
		counter_2 = 0;
		counter_3 = 0;

		for(j=i;j<i+8;j++)
		{
			if( m_nCardsTable[j] == 1 )
			{
				counter_1++;
			}
			else if( m_nCardsTable[j] == 2 )
			{
				counter_2++;
			}
			else if( m_nCardsTable[j] == 3 )
			{
				counter_3++;
			}
			else
			{
				break;  //如果没有牌或者是四张(炸弹),则不考虑;
			}
		}
		//看连续牌数是否等于8;
		if( counter_1 == 5 && counter_2 + counter_3 == 3 ||
			counter_1 == 6 && counter_2 + counter_3 == 2 ||
			counter_1 == 7 && counter_2 + counter_3 == 1 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 8;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 8;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<8;l++)
					{
						if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
						{
							k++;
							if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
							{
								k++;
							}
						}

						m_cDiscarding[l] = m_cCards[k + l];
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l]--;
					}
				}				
			}
			return 1; //搞定,返回;
		}
	}//end for~;
	return 0;
}
//寻找9单顺出;
int CPlayingCards::Search9(CCardsType& Cards_Type)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	int i,j,k,l;
	//连续单牌的个数;
	int counter_1;
	int counter_2;
	int counter_3;

	//如果是10,j,q,k,A,则考虑对手中牌的个数,如果太少就考虑寻找炸弹;

	if( Cards_Type.m_nTypeValue >= 6 ) 
	{
		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=6;i++)
	{
		//第一遍扫描,看是否有全单的单顺;
	    counter_1 = 0;
		for(j=i;j<i+9;j++)
		{
			if( m_nCardsTable[j] != 1 )
			{
				break;
			}
			else
			{
				counter_1++;
			}
		}
		//看连续牌数是否等于9;
		if( counter_1 == 9 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 9;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 9;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<9;l++)
					{
						m_cDiscarding[l] = m_cCards[k + l];
				
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l] = 0;
					}
				}				
			}
			return 1; //搞定,返回;
		}

		//第二遍扫描;
	    counter_1 = 0;
		counter_2 = 0;
		counter_3 = 0;

		for(j=i;j<i+9;j++)
		{
			if( m_nCardsTable[j] == 1 )
			{
				counter_1++;
			}
			else if( m_nCardsTable[j] == 2 )
			{
				counter_2++;
			}
			else if( m_nCardsTable[j] == 3 )
			{
				counter_3++;
			}
			else
			{
				break;  //如果没有牌或者是四张(炸弹),则不考虑;
			}
		}
		//看连续牌数是否等于8;
		if( counter_1 == 6 && counter_2 + counter_3 == 3 ||
			counter_1 == 7 && counter_2 + counter_3 == 2 ||
			counter_1 == 8 && counter_2 + counter_3 == 1 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = 9;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = 9;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<9;l++)
					{
						if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
						{
							k++;
							if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
							{
								k++;
							}
						}

						m_cDiscarding[l] = m_cCards[k + l];
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l]--;
					}
				}				
			}
			return 1; //搞定,返回;
		}
	}//end for~;
	return 0;
}
//寻找10单顺出;
int CPlayingCards::SearchBeyond10(CCardsType& Cards_Type,int Long)
{
	if( AI_Patch1() )
	{
		return 1;
	}
	int i,j,k,l;
	//连续单牌的个数;
	int counter_1;
	int counter_2;
	int counter_3;

	//如果是10,j,q,k,A,则考虑对手中牌的个数,如果太少就考虑寻找炸弹;
//	CString str;
//	str.Format("Cards_Type.m_nTypeNum = %d\nCards_Type.m_nTypeValue = %d\n",\
//					Cards_Type.m_nTypeNum,Cards_Type.m_nTypeValue);
//	AfxMessageBox(str);

	if( Cards_Type.m_nTypeValue > 14 - Long ) 
	{

		return 0;
	}

	for(i=Cards_Type.m_nTypeValue+1;i<=14-Long+1;i++)
	{
		//第一遍扫描,看是否有全单的单顺;
	    counter_1 = 0;
		for(j=i;j<i+Long;j++)
		{
			if( m_nCardsTable[j] != 1 )
			{
				break;
			}
			else
			{
				counter_1++;
			}
		}
		//看连续牌数是否等于Long;
		if( counter_1 == Long )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = Long;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = Long;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<Long;l++)
					{
						m_cDiscarding[l] = m_cCards[k + l];
				
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l] = 0;
					}
				}				
			}
			return 1; //搞定,返回;
		}

		//第二遍扫描;
	    counter_1 = 0;
		counter_2 = 0;
		counter_3 = 0;

		for(j=i;j<i+Long;j++)
		{
			if( m_nCardsTable[j] == 1 )
			{
				counter_1++;
			}
			else if( m_nCardsTable[j] == 2 )
			{
				counter_2++;
			}
			else if( m_nCardsTable[j] == 3 )
			{
				counter_3++;
			}
			else
			{
				break;  //如果没有牌或者是四张(炸弹),则不考虑;
			}
		}
		//看连续牌数是否等于8;
		if( counter_1 == Long-4 && counter_2 + counter_3 == 4 ||
			counter_1 == Long-3 && counter_2 + counter_3 == 3 ||
			counter_1 == Long-2 && counter_2 + counter_3 == 2 ||
			counter_1 == Long-1 && counter_2 + counter_3 == 1 )
		{
			//其他相关判断;
			//出牌处理;
			m_cDiscardingType.m_nTypeNum   = Long;
			m_cDiscardingType.m_nTypeValue = i;

			m_nDiscardingCounter = Long;

			for(k=0;k<m_nCardsCounter;k++)
			{
				if( m_cCards[k].m_nValue == i )
				{
					for(l=0;l<Long;l++)
					{
						if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
						{
							k++;
							if( m_cCards[k + l].m_nValue == m_cCards[k + l + 1].m_nValue )
							{
								k++;
							}
						}

						m_cDiscarding[l] = m_cCards[k + l];
						m_cCards[k + l] = m_cBLANK_CARD;

						m_nCardsTable[i + l]--;
					}
				}				
			}
			return 1; //搞定,返回;
		}
	}//end for~;
	return 0;
}
//比较大小后决定不要或者出牌; 
int CPlayingCards::CompareChoose(CCardsType& Cards_Type)
{
//		CString str;
//		str.Format("Cards_Type.m_nTypeNum = %d\nCards_Type.m_nTypeValue = %d\n",\
//					Cards_Type.m_nTypeNum,Cards_Type.m_nTypeValue);
//		AfxMessageBox(str);
//		Sleep(1000);

	switch( Cards_Type.m_nTypeNum )
	{
	//选择一个单牌;
	case 1:
		if( Search1(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 2:
		if( Search2(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 3:
		if( Search3(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}	
	case 4:
		if( SearchBomb(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 31:
		if( Search31(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}	
	case 32:
		if( Search32(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 411:
		if( Search411(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 422:
		if( Search422(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 3311:
		if( Search3311(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 3322:
		if( Search3322(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 5:
		if( Search5(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 6:
		if( Search6(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 7:
		if( Search7(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 8:
		if( Search8(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 9:
		if( Search9(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	case 10:
		if( SearchBeyond10(Cards_Type,10) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 11:
		if( SearchBeyond10(Cards_Type,11) )
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	case 12:
		if( SearchBeyond10(Cards_Type,12) )
		{

			return 1;
		}
		else
		{
			return 0;
		}
	case 222:
		if( Search222(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	case 2222:
		if( Search2222(Cards_Type) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	return 0;
}

//计算牌值;
int CPlayingCards::AccountCardsValue()
{
	int Cards_Value = 0;

	switch( m_nCardsTable[16]	)
	{
	case 1:
		if( m_cCards[16].m_nColor == 0 ) //大王;
		{
			Cards_Value += 80;
		}
		else
		{
			Cards_Value += 40;
		}
		break;
	case 2:
		Cards_Value += 200;
		break;
	}

	switch( m_nCardsTable[15] )
	{
	case 1:
		Cards_Value += 20;
		break;
	case 2:
		Cards_Value += 40;
		break;
	case 3:
		Cards_Value += 60;
		break;
	case 4:
		Cards_Value += 200;
		break;
	}

	switch( m_nCardsTable[14] )
	{
	case 1:
		Cards_Value += 5;
		break;
	case 2:
		Cards_Value += 10;
		break;
	case 3:
		Cards_Value += 30;
		break;
	case 4:
		Cards_Value += 150;
		break;
	}

	for( int i=3;i<14;i++)
	{
		if( m_nCardsTable[i] == 4 )
		{
			Cards_Value += 100 + i*5;
		}
	}

	return Cards_Value;
}

void CPlayingCards::ScanToTable()
{
	//把两个电脑玩家的牌扫描进表中;
	for(int i=3;i<17;i++)
	{
		m_nCardsTable[i] = 0;
	}
	for(i=0;i<m_nCardsCounter;i++)
		m_nCardsTable[m_cCards[i].m_nValue]++;
}