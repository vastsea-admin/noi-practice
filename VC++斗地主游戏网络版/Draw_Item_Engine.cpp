//Download by http://www.NewXing.com
// Draw_Item_Engine.cpp: implementation of the CDrawItemEngine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "斗地主.h"
#include "MainFrm.h"

#include "My_DirectInput.h"
#include "My_directdraw.h"

#include "Game.h"

#include "Draw_Cards_Engine.h"
#include "Draw_Item_Engine.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDrawItemEngine::CDrawItemEngine()
{
	//精灵颜色键;
	blue = _RGB32BIT(0,0,0,255);
}

CDrawItemEngine::~CDrawItemEngine()
{

}
//

//相关初始化;
int CDrawItemEngine::Init()
{
	char* file;
//游戏界面离屏表面的建立;
	file = "bmp\\Game_Back.bmp" ;
	lpddsbg_Game = pDraw->CreateSurface(Screen_Width, Screen_Height,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Game,0,0,file,Screen_Width,Screen_Height);

//face;	
	file = "bmp\\faces\\face0.bmp";
	lpddsbg_Person[0] = pDraw->CreateSurface(70,70,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Person[0],0,0,file,70,70);

	file = "bmp\\faces\\face1.bmp";
	lpddsbg_Person[1] = pDraw->CreateSurface(70,70,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Person[1],0,0,file,70,70);

	file = "bmp\\faces\\face2.bmp";
	lpddsbg_Person[2] = pDraw->CreateSurface(70,70,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Person[2],0,0,file,70,70);

	file = "bmp\\faces\\face3.bmp";
	lpddsbg_Person[3] = pDraw->CreateSurface(70,70,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Person[3],0,0,file,70,70);
	//左边玩家头像;
	pDraw->DrawSurface(lpddsbg_Person[pLeftPlayer->m_nFaceID], 
								40, 30,
								70,70,
								lpddsbg_Game,0);
	//中间玩家头像;
	pDraw->DrawSurface(lpddsbg_Person[pCenterPlayer->m_nFaceID], 
								40, 620,
								70,70,
								lpddsbg_Game,0);
	//右边玩家头像;
	pDraw->DrawSurface(lpddsbg_Person[pRightPlayer->m_nFaceID], 
								910, 30,
								70,70,
								lpddsbg_Game,0);
	//玩家名字;
	pDraw->TextGDI(pLeftPlayer->m_szName,45,110,RGB(0,0,225),160,"华文新魏",lpddsbg_Game);
	pDraw->TextGDI(pCenterPlayer->m_szName,45,700,RGB(0,0,225),160,"华文新魏",lpddsbg_Game);
	pDraw->TextGDI(pRightPlayer->m_szName,915,110,RGB(0,0,225),160,"华文新魏",lpddsbg_Game);

	pDraw->TextGDI(pLeftPlayer->m_szName,144,658,RGB(255,0,0),130,"华文新魏",lpddsbg_Game);
	pDraw->TextGDI(pCenterPlayer->m_szName,144,685,RGB(255,0,0),130,"华文新魏",lpddsbg_Game);
	pDraw->TextGDI(pRightPlayer->m_szName,144,710,RGB(255,0,0),130,"华文新魏",lpddsbg_Game);

	//载入其他界面相关项;

	//产生随机地主的动画框;
	file = "bmp\\item\\bring_lord.bmp";
	lpddsbg_Menu[0] = pDraw->CreateSurface(300,150,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Menu[0],0,0,file,300,150);
    //头像外框;
	file = "bmp\\item\\lord_frame.bmp";
	lpddsbg_Menu[1] = pDraw->CreateSurface(76,76,0,blue);
	pDraw->BitmapToSurface(lpddsbg_Menu[1],0,0,file,76,76);
	//选择地主的菜单框;
	file = "bmp\\item\\choose_lord.bmp";
	lpddsbg_Menu[2] = pDraw->CreateSurface(200,80,0,blue);
	pDraw->BitmapToSurface(lpddsbg_Menu[2],0,0,file,200,80);
	//出牌按钮;
	file = "bmp\\item\\Ok_Button.bmp";
	lpddsbg_Menu[3] = pDraw->CreateSurface(80,30,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Menu[3],0,0,file,80,30);

	file = "bmp\\item\\Pass_Button.bmp";
	lpddsbg_Menu[4] = pDraw->CreateSurface(80,30,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Menu[4],0,0,file,80,30);
	//得分表框;
	file = "bmp\\item\\show_score.bmp";
	lpddsbg_Menu[5] = pDraw->CreateSurface(300,250,0,-1);
	pDraw->BitmapToSurface(lpddsbg_Menu[5],0,0,file,300,250);

	pDraw->DrawSurface(lpddsbg_Game,0,0, Screen_Width,Screen_Height,lpddsback,0); 
	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));	

	return 1;
}
//显示第几局的函数;
int CDrawItemEngine::GameCounter(int nGameCounter)
{
	pDraw->DrawSurface(lpddsbg_Game,0,0, Screen_Width,Screen_Height,lpddsback,0);
//刷新屏幕;
	char info[30];
	sprintf(info,"第%d局开始",nGameCounter);
	pDraw->TextGDI(info,360,200,RGB(255,0,0),500,"隶书",lpddsback);

	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));	

	PlaySound(MAKEINTRESOURCE(IDR_START),AfxGetResourceHandle(),
						SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //	
	Sleep(1000);	
	return 1;
}
//
int CDrawItemEngine::BringRandLord(int nDefaultLord)
{
//	pDraw->DrawSurface(lpddsbg_Game,0,0, Screen_Width,Screen_Height,lpddsback,0); 
	int counter = 0;

	while( counter < 10 )
	{
	PlaySound(MAKEINTRESOURCE(IDR_DING),AfxGetResourceHandle(),
				SND_ASYNC|SND_RESOURCE|SND_NODEFAULT );  //
	::RedrawGame(NULL);
	pDraw->DrawSurface(lpddsbg_Menu[0], 
								365, 300,
								300,150,
								lpddsback,0);

	//左边玩家;
	pDraw->DrawSurface(lpddsbg_Person[pLeftPlayer->m_nFaceID], 
								390, 320,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pLeftPlayer->m_szName,400,400,RGB(0,0,0),200,"隶书",lpddsback);
	//中间玩家;
	pDraw->DrawSurface(lpddsbg_Person[pCenterPlayer->m_nFaceID], 
								480, 320,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pCenterPlayer->m_szName,490,400,RGB(0,0,0),200,"隶书",lpddsback);
	//右边玩家;
	pDraw->DrawSurface(lpddsbg_Person[pRightPlayer->m_nFaceID], 
								570, 320,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pRightPlayer->m_szName,580,400,RGB(0,0,0),200,"隶书",lpddsback);

	//显示字:
	pDraw->TextGDI("正在产生随机地主...",330,230,RGB(0,0,0),300,"隶书",lpddsback);

	switch( rand()%3 )
	{
	case 0:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								390-3, 320-3,
								76,76,
								lpddsback,1);
		break;
	case 1:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								480-3, 320-3,
								76,76,
								lpddsback,1);
		break;
	case 2:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								570-3, 320-3,
								76,76,
								lpddsback,1);
	}	
	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));	
	counter++;
	Sleep(150);	
	}


//生产地主后的信息提示:
	::RedrawGame(NULL);
	pDraw->DrawSurface(lpddsbg_Menu[0], 
								365, 300,
								300,150,
								lpddsback,0);

	//左边玩家;
	pDraw->DrawSurface(lpddsbg_Person[pLeftPlayer->m_nFaceID], 
								390, 320,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pLeftPlayer->m_szName,400,400,RGB(0,0,0),200,"隶书",lpddsback);
	//中间玩家;
	pDraw->DrawSurface(lpddsbg_Person[pCenterPlayer->m_nFaceID], 
								480, 320,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pCenterPlayer->m_szName,490,400,RGB(0,0,0),200,"隶书",lpddsback);
	//右边玩家;
	pDraw->DrawSurface(lpddsbg_Person[pRightPlayer->m_nFaceID], 
								570, 320,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pRightPlayer->m_szName,580,400,RGB(0,0,0),200,"隶书",lpddsback);
	//选出地主提示:
	char lord_info[50];	
	switch( nDefaultLord )
	{
	case 0:
		sprintf(lord_info,"本局的随机地主是:%s",pLeftPlayer->m_szName);
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								390-3, 320-3,
								76,76,
								lpddsback,1);
		break;
	case 1:
		sprintf(lord_info,"本局的随机地主是:%s",pCenterPlayer->m_szName);
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								480-3, 320-3,
								76,76,
								lpddsback,1);
		break;
	case 2:
		sprintf(lord_info,"本局的随机地主是:%s",pRightPlayer->m_szName);
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								570-3, 320-3,
								76,76,
								lpddsback,1);
		break;
	}

//	AfxMessageBox(lord_info);
	pDraw->TextGDI(lord_info,330,230,RGB(0,0,0),300,"隶书",lpddsback);
	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));
	Sleep(1500);
	return 1;
}
//产生随机地主的动画;
int CDrawItemEngine::ChooseLord()
{
	//选择地主效果;
	::RedrawGame(NULL);
	pDraw->DrawSurface(lpddsbg_Menu[2], 
								320, 500,
								200,80,
								lpddsback,0);

	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));	
//	Sleep(5000);

	return 1;
}
//显示游戏过程中选地主的信息;
int CDrawItemEngine::GameInfo(int nPlayer,int nInfoType)
{
	//info_type = 0: "%s要当地主,本局地主是:%s"
	//info_type = 1: "%s不当地主,等待%s选择..."
	//info_type = 2: "按规则,本局地主是:%s"
	//	
	::RedrawGame(NULL);

	char lord_info[50];
	switch( nInfoType )
	{
	case 0:
		switch( nPlayer )  //当前地主当;
		{
		case 0:
			sprintf(lord_info,"%s要当地主,本局地主是:%s",\
				pLeftPlayer->m_szName,pLeftPlayer->m_szName);
			break;
		case 1:
			sprintf(lord_info,"%s要当地主,本局地主是:%s",\
				pCenterPlayer->m_szName,pCenterPlayer->m_szName);
			break;
		case 2:
			sprintf(lord_info,"%s要当地主,本局地主是:%s",\
				pRightPlayer->m_szName,pRightPlayer->m_szName);
			break;
		}
		break;
	case 1:
		switch( nPlayer )
		{
		case 0:
			sprintf(lord_info,"%s不当地主,等待%s选择...",\
				pLeftPlayer->m_szName,pCenterPlayer->m_szName);
			break;
		case 1:
			sprintf(lord_info,"%s不当地主,等待%s选择...",\
				pCenterPlayer->m_szName,pRightPlayer->m_szName);
			break;
		case 2:
			sprintf(lord_info,"%s不当地主,等待%s选择...",\
				pRightPlayer->m_szName,pLeftPlayer->m_szName);
			break;
		}
		break;
	case 2:
		switch( nPlayer )
		{
		case 0:
			sprintf(lord_info,"按规则,本局地主是:%s",\
				pLeftPlayer->m_szName);
			break;
		case 1:
			sprintf(lord_info,"按规则,本局地主是:%s",\
				pCenterPlayer->m_szName);
			break;
		case 2:
			sprintf(lord_info,"按规则,本局地主是:%s",\
				pRightPlayer->m_szName);
			break;
		}
		break;
	}
	pDraw->TextGDI(lord_info,280,300,RGB(0,0,255),250,"隶书",lpddsback);
	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));
	Sleep(1000);
	return 1;
}
//显示得分情况;
int CDrawItemEngine::GameScore(int nCurrentLord,int nLeft,int nCenter,int nRight)
{
	char score_info[10];

	::RedrawGame(NULL);
	pDraw->DrawSurface(lpddsbg_Menu[5], 
								365, 200,
								300,250,
								lpddsback,0);

	//左边玩家;
	pDraw->DrawSurface(lpddsbg_Person[pLeftPlayer->m_nFaceID], 
								390, 270,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pLeftPlayer->m_szName,400,350,RGB(0,0,0),200,"隶书",lpddsback);
	//显示得分;
	sprintf(score_info,"%d",nLeft);
	pDraw->TextGDI(score_info,410,380,RGB(255,0,0),300,"华文新魏",lpddsback);	
	//中间玩家;
	pDraw->DrawSurface(lpddsbg_Person[pCenterPlayer->m_nFaceID], 
								480, 270,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pCenterPlayer->m_szName,490,350,RGB(0,0,0),200,"隶书",lpddsback);
	//显示得分;
	sprintf(score_info,"%d",nCenter);
	pDraw->TextGDI(score_info,500,380,RGB(255,0,0),300,"华文新魏",lpddsback);
	//右边玩家;
	pDraw->DrawSurface(lpddsbg_Person[pRightPlayer->m_nFaceID], 
								570, 270,
								70,70,
								lpddsback,0);
	pDraw->TextGDI(pRightPlayer->m_szName,580,350,RGB(0,0,0),200,"隶书",lpddsback);
	//显示得分;
	sprintf(score_info,"%d",nRight);
	pDraw->TextGDI(score_info,590,380,RGB(255,0,0),300,"华文新魏",lpddsback);

	switch( nCurrentLord )
	{
	case 0:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								390-3, 270-3,
								76,76,
								lpddsback,1);
		break;
	case 1:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								480-3, 270-3,
								76,76,
								lpddsback,1);
		break;
	case 2:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
								570-3, 270-3,
								76,76,
								lpddsback,1);
	}	
	while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));	

	Sleep(3000);
	return 1;
}

void CDrawItemEngine::Redraw(LPDIRECTDRAWSURFACE7  pSurface)
{
	if( pCardsMap->m_nUpCounter == 0 )
	{
		pDraw->DrawSurface(lpddsbg_Menu[4], 
								660, 550,
								80,30,
								lpddsback,0);
	}
	else
	{
		pDraw->DrawSurface(lpddsbg_Menu[3], 
								660, 550,
								80,30,
								lpddsback,0);
	}

	switch( pGame->m_nCurrentLord )
	{
	case 0:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
							40-3, 30-3,
							76,76,
							lpddsback,1);
		break;
	case 1:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
							40-3, 620-3,
							76,76,
							lpddsback,1);
		break;
	case 2:
		pDraw->DrawSurface(lpddsbg_Menu[1], 
							910-3, 30-3,
							76,76,
							lpddsback,1);
		break;
	}

	char table_info[100];
	//左边玩家得分信息;
	sprintf(table_info,"%d        %d         %d",pLeftPlayer->m_nScore,pLeftPlayer->m_nWin,pLeftPlayer->m_nLose);
	pDraw->TextGDI(table_info,210,658,RGB(255,0,0),130,"华文新魏",lpddsback);
	//中间玩家得分信息;
	sprintf(table_info,"%d        %d         %d",pCenterPlayer->m_nScore,pCenterPlayer->m_nWin,pCenterPlayer->m_nLose);
	pDraw->TextGDI(table_info,210,685,RGB(255,0,0),130,"华文新魏",lpddsback);
	//右边玩家得分信息;
	sprintf(table_info,"%d        %d         %d",pRightPlayer->m_nScore,pRightPlayer->m_nWin,pRightPlayer->m_nLose);
	pDraw->TextGDI(table_info,210,710,RGB(255,0,0),130,"华文新魏",lpddsback);


}
